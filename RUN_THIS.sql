USE HackCity;

DROP DATABASE IF EXISTS HackCity;
CREATE DATABASE HackCity;
USE HackCity;

CREATE TABLE Users (
	username VARCHAR(42) NOT NULL PRIMARY KEY,
	pass VARCHAR(42) NOT NULL
);

CREATE TABLE GameStatistics (
	username VARCHAR(42) NOT NULL PRIMARY KEY,
    gamesPlayed int,
    gamesWon int,
    gamesLost int,
    gamesAsWhiteHat int,
    gamesAsBlackHat int,
    gamesAsGreyHat int,
    FOREIGN KEY fk(username) REFERENCES Users(username)
);

INSERT INTO Users (username, pass)
VALUES ('avery','3e10e87da0f93b981c31e6dab6f5fdb7'),
	('meag', '3e10e87da0f93b981c31e6dab6f5fdb7'), # decrypts to 'avery'
	('tim', '3e10e87da0f93b981c31e6dab6f5fdb7'),
    ('tiff', '3e10e87da0f93b981c31e6dab6f5fdb7'),
    ('eric', '3e10e87da0f93b981c31e6dab6f5fdb7');

INSERT INTO gamestatistics (username)
VALUES ('meag'),
('tim'),
('tiff'),
('eric'),
('avery');

UPDATE GameStatistics
SET gamesPlayed=0, gamesWon=0, gamesLost=0, gamesAsWhiteHat=0, gamesAsGreyHat=0, gamesAsBlackHat=0
WHERE username='meag';

UPDATE GameStatistics
SET gamesPlayed=0, gamesWon=0, gamesLost=0, gamesAsWhiteHat=0, gamesAsGreyHat=0, gamesAsBlackHat=0
WHERE username='tim';

UPDATE GameStatistics
SET gamesPlayed=0, gamesWon=0, gamesLost=0, gamesAsWhiteHat=0, gamesAsGreyHat=0, gamesAsBlackHat=0
WHERE username='tiff';

UPDATE GameStatistics
SET gamesPlayed=0, gamesWon=0, gamesLost=0, gamesAsWhiteHat=0, gamesAsGreyHat=0, gamesAsBlackHat=0
WHERE username='avery';

UPDATE GameStatistics
SET gamesPlayed=0, gamesWon=0, gamesLost=0, gamesAsWhiteHat=0, gamesAsGreyHat=0, gamesAsBlackHat=0
WHERE username='eric';