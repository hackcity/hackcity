package dataTypes;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Serializable;
import java.util.Random;
import java.util.Scanner;

//Card class holds the elements representing a card in the game
public class Card implements Serializable {
	private static final long serialVersionUID = -8473530995389784513L;
	public static final String DataFilename = "CardData.txt";
	public static final String ImageFolder = "CardImages/";
	public static final String png = ".png";
	public static final String srcFolder = "src/";
	public static int NumCardTypes = -1;
	public static final int firstAvatarCardID = 34;
	public static final int numberOfAvatarCards = 7;

	private int ID;
	private String title;
	private String flavorText;
	private String tooltipText;
	private boolean playOnOthers;
	private boolean playAnytime;

	//Initializes all the variables.
	//ID 30, 31, 32, 33 are hat cards
	//ID 34+ are avatar cards.
	public Card(int id) {

		//If we don't know the total number of cards, calculate it
		if (NumCardTypes < 0) {
			countNumCardTypes();
		}

		//Actual Constructor starts here
		if (id == -1) {
			this.title = "";
			this.flavorText = "";
			this.tooltipText = "";
		} else {
			//Gets information from CardData.txt
			boolean okay = true;
			Scanner scanner = null;
			try {
				scanner = new Scanner(new BufferedReader(new FileReader(new File(srcFolder + Card.DataFilename))));
				//Skip all lines that are irrelevant
				for (int i = 1; i < id; i++) {
					scanner.nextLine();
				}
				if (okay == true) {
					if (scanner.hasNextLine()) {
						String line = scanner.nextLine();
						if(line.startsWith("#")) {
							//Comment
							title = "INVALID ID";
							flavorText = "THIS LINE IS A COMMENT";
							tooltipText = "WRONG";							
						}else if (line.startsWith("@")) {
							//Hat card
							line = line.substring(1);
							Scanner lineScanner = new Scanner(line);
							lineScanner.useDelimiter(";");
							title = lineScanner.next();
							flavorText = lineScanner.next();
							tooltipText = lineScanner.next();
							lineScanner.close();
						}else if (line.startsWith("!")) {
							//Avatar card
							line = line.substring(1);
							Scanner lineScanner = new Scanner(line);
							lineScanner.useDelimiter(";");
							title = lineScanner.next();
							flavorText = lineScanner.next();
							tooltipText = lineScanner.next();
							lineScanner.close();
						} else {
							//Read in the line we want
							Scanner lineScanner = new Scanner(line);
							lineScanner.useDelimiter(";");
							title = lineScanner.next();
							flavorText = lineScanner.next();
							tooltipText = lineScanner.next();
							String s = lineScanner.next();
							if (s.equals(" 1")) {
								playOnOthers = true;
							} else {
								playOnOthers = false;
							}
							s = lineScanner.next();
							if (s.equals(" 1"))
								playAnytime = true;
							else
								playAnytime = false;
							lineScanner.close();
						}
					} else {
						okay = false;
					}
				}
				//ID not found
				if (okay == false) {
					System.out.println("CAN'T FIND ID:" + id + " in " + Card.DataFilename);
				}
			} catch (FileNotFoundException e) {
				System.out.println("Can't find " + Card.DataFilename);
				e.printStackTrace();
			} finally {
				if (scanner != null) {
					scanner.close();
				}
			}
		}
	}
	
	//Get a random card
	public static Card getRandomCard() {
		Random rand = new Random();
		if(NumCardTypes < 0 ) {
			countNumCardTypes();
		}
		int i = rand.nextInt(NumCardTypes+1)+1;
		//Double the amount of hack cards.
		i--;
		if(i == 0) {
			i = 1;
		}
		//End hack card doubling
		Card c = new Card(i);
		return c;
	}

	//Goes into the file and statically counts the number of cards there are
	//essentially works like a .size() function
	public static void countNumCardTypes() {
		int count = 0;
		Scanner scanner = null;
		try {
			scanner = new Scanner(new BufferedReader(new FileReader(new File(srcFolder + Card.DataFilename))));
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (line.startsWith("@") || line.startsWith("#") || line.startsWith("!")) {
					continue;
				}
				//Only count normal cards
				count++;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		NumCardTypes = count;
	}

	//Get card ID
	public int getID() {
		return ID;
	}

	//Get title
	public String getTitle() {
		return title;
	}

	//Get flavor text
	public String getFlavorText() {
		return flavorText;
	}

	//Get tooltip text
	public String getTooltipText() {
		return tooltipText;
	}

	//Returns status of card if able to play on others
	public boolean canPlayOnOthers() {
		return playOnOthers;
	}

	//Returns status of card if can play anytime
	public boolean canPlayAnytime() {
		return playAnytime;
	}

	//Compares cards
	public boolean equals(Card other) {
		return (this.getID() == other.getID());
	}
	
	//To string for card
	public String toString() {
		String s = "";
		s += this.title + " : " ;
		s += this.getFlavorText() + " : ";
		s += this.getTooltipText();
		return s;
	}
}
