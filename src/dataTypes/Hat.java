package dataTypes;
import java.io.Serializable;

//Hat of the player (White, Grey, Black)
public class Hat implements Serializable{
	private static final long serialVersionUID = -1958355607226462491L;
	public static final int PHD = 2;
	public static final int phd = 2;
	public static final int WHITE = 1;
	public static final int white = 1;
	public static final int GRAY = 0;
	public static final int gray = 0;
	public static final int GREY = 0;
	public static final int grey = 0;
	public static final int BLACK = -1;
	public static final int black = -1;
	
	public static final int cardIDPHD = 30;
	public static final int cardIDWhite = 31;
	public static final int cardIDGray = 32;
	public static final int cardIDGrey = 32;
	public static final int cardIDBlack = 33;
	
	public static final int NONE = -123;
	public static final int none = -123;

	private int type;
	
	//Constructor
	Hat(int i) {
		this.type = i;
	}

	//Returns the tpye of hat
	private int type() {
		return this.type;
	}

	//Returns if the hats are on the same team (checking Hat)
	public boolean sameTeam(Hat other) {
		if (this.type > 0 && other.type > 0){
			return true;
		}
		return this.type == other.type();
	}
	
	//Returns if the hats on the same team (checking hat type)
	public boolean sameTeam(int hatType) {
		if (this.type > 0 && hatType > 0){
			return true;
		}
		return this.type == hatType;
	}

	//Check if hats are of same type
	public boolean equals(Hat other) {
		return this.type == other.type();
	}
	
	//Checks if hats are of same type
	public boolean equals(int hatType) {
		return this.type == hatType;
	}
	
	//Get the hat ID of the card
	public int getCardID() {
		if(this.type == 2) {
			return Hat.cardIDPHD;
		} else if (this.type == 1) {
			return Hat.cardIDWhite;
		} else if (this.type == 0) {
			return Hat.cardIDGray;
		} else if (this.type == -1) {
			return Hat.cardIDBlack;
		}
		return -1;
	}
	
	//toString function for hats
	public String toString() {
		String s = "";
		switch(this.type) {
			case 2: s += "PHD";	break;
			case 1: s += "White"; break;
			case 0: s += "Gray"; break;
			case -1: s += "Black"; break;
			default: s += "none"; break;
		}
		return s;
	}
}
