package dataTypes;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//Players
public class Player implements Serializable {
	
	private static final long serialVersionUID = 8563410328045057417L;
	private String username;
	private int health;
	private int maxHealth;
	private Hat hat;
	private Card avatar;
	private List<Card> equippedCards;
	private List<Card> hand;
	boolean alive;
	boolean isGuest;

	//Constructor
	Player() {
		initializeVariables();
	}

	//Constructor
	public Player(String username) {
		initializeVariables();
		this.setUsername(username);
	}

	//Initialize variables
	private void initializeVariables() {
		//TODO fix this shit
		health = 4;
		maxHealth = 4;
		
		equippedCards = new ArrayList<Card>();
		hand = new ArrayList<Card>();
		alive = true;
		isGuest = true;
		
	}

	//Set username of player
	public void setUsername(String username) {
		this.username = username;
	}

	//Get username of player
	public String getUsername() {
		return this.username;
	}

	//Get player health
	public int getHealth() {
		return this.health;
	}

	//Set maximum health of player
	public void setMaxHealth(int maxHealth) {
		this.maxHealth = maxHealth;
		this.changeHealth(0); // if health too high, change it
	}

	//Get maximum health of player
	public int getMaxHealth() {
		return this.maxHealth;
	}

	//Set health of player (for testing)
	public void setHealth(int i) {
		health = i;
	}

	//Change health of player
	public void changeHealth(int i) {
		health += i;
		if (health > maxHealth) {
			health = maxHealth;
		}
		if (health < 0) {
			health = 0;
		}
		if (health == 0) {
			alive = false;
		}
	}

	//Set hat of player
	public void setHat(Hat hat) {
		this.hat = hat;
	}

	//Get hat of player
	public Hat getHat() {
		return this.hat;
	}

	//Set avatar of player
	public void setAvatar(Card avatar) {
		this.avatar = avatar;
	}

	//Get avatar of player
	public Card getAvatar() {
		return avatar;
	}

	//Get hand size
	public int handSize() {
		return hand.size();
	}

	//Get list of cards in hand
	public List<Card> getHand() {
		return hand;
	}

	//Get list of equipped cards
	public List<Card> getEquippedCards() {
		return equippedCards;
	}

	//Add a card to hand
	public void giveCard(Card newCard) {
		hand.add(newCard);
	}

	//Equip a card to hand
	public void equipCard(Card newCard) {
		equippedCards.add(newCard);
	}

	//Remove card from hand using Card
	public boolean removeCardFromHand(Card card) {	
		for(int i=0; i<hand.size(); i++){
			if(card.getTitle().equals(hand.get(i).getTitle())){
				hand.remove(i);
				
				return true;
			}
		}
	
		return false;
	}
	
	//Remove card from hand using index
	public void removeCardFromHand(int index) {
		hand.remove(index);
	}

	//Remove card from equip
	public boolean removeCardFromEquip(Card card) {
		return equippedCards.remove(card);
	}

	//Check if player alive
	public boolean isAlive() {
		return alive;
	}

	//Set player as guest
	public void setIsGuest(boolean b) {
		isGuest = b;
	}

	//Check if player is guest
	public boolean isGuest() {
		return isGuest;
	}

	//Check if players are on same team
	public static boolean sameTeam(List<Player> players) {
		Hat hat = players.get(0).getHat();
		for (Player player : players) {
			if (!(player.getHat().sameTeam(hat))) {
				// there is a player from a different team
				return false;
			}
		}
		return true;
	}
	
	//Check if sheriff is dead
	public static boolean sheriffIsDead(List<Player>players) {
		boolean foundSheriff = false;
		for (Player p : players) {
			if (p.getHat().equals(Hat.phd)) {
				foundSheriff = true;;
			}
		}
		//If cannot find sheriff, sheriff is dead
		if (foundSheriff == false) {
			return true;
		} else {
			//Found a sheriff, so return false
			return false;
		}
	}
	
	//Compares Players
	public boolean equals(Player player) {
		if(getUsername().equals(player.getUsername())) {
			//System.out.println("EQUAL");
			return true;
		} else {
			//System.out.println("NOT EQUAL");
			return false;
		}
	}
	
	//Draws a random card
	public boolean drawRandomCard(){
		boolean cardDeleted = false;
		while(this.getHand().size() >= this.getHealth()) {
			//When you draw a card will need to be thrown out;
			this.getHand().remove(0);
			cardDeleted = true;
		} 
		//Give a card to players hand at the end of the hand.
		this.getHand().add(Card.getRandomCard());
		return cardDeleted;
	}
	
	//Print hand
	public void printHand() {
		System.out.println(this.getUsername()+ "'s Hand:");
		for(int i = 0; i < this.getHand().size(); ++i) {
			System.out.println("\t" + this.getHand().get(i));
		}
	}
 
	//toString function for Players
	public String toString() {
		String s = "";
		s += username + ", ";
		s += "Hat: " + this.getHat().toString() + ", ";
		s += "HP: " + this.getHealth() + "/" + this.getMaxHealth() + ", ";
		s += "Hand: " + this.getHand().size() + ", ";
		s += "Equip: " + this.getEquippedCards().size() + ", ";
		s += "Alive: " + this.isAlive();
		return s;
	}
}
