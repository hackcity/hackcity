package dataTypes;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

//GameState of the game (refreshes every turn)
public class GameState implements Serializable {
	private static final long serialVersionUID = 2294268101141884083L;
	private List<Player> players;
	private List<Card> tableCards;
	private Hat winningHat;
	private Random rand;
	private Player currentPlayer;
	
	//Constructor
	public GameState(Vector<Player> players) {
		initializeVariables();
	
		//Initialize all the players
		initializePlayers(players);
		
		//Add new (NULL) card to tableCards
		tableCards.add(new Card(-1));
	}

	//Initialize variables
	private void initializeVariables() {
		players = new ArrayList<Player>();
		tableCards = new ArrayList<Card>();
		winningHat = new Hat(Hat.none);
		rand = new Random();
	}
	
	//Initialize players
	private void initializePlayers(Vector<Player> players){
		//This creates a pool of hats to draw from, 
		//and when we draw from this pool we always use remove, 
		//so there will be the right number of each hat in the game overall
		ArrayList<Hat> hats = new ArrayList<Hat>();
		int n = players.size();
		hats.add(new Hat(Hat.PHD));
		hats.add(new Hat(Hat.GRAY));
		while(true) {
			hats.add(new Hat(Hat.black));
			if(hats.size() >= n) break;
			hats.add(new Hat(Hat.white));
			if(hats.size() >= n) break;
		}
		
		ArrayList<Integer> avatars = new ArrayList<Integer>();
		for(int i = Card.firstAvatarCardID; i < Card.firstAvatarCardID+Card.numberOfAvatarCards; i++) {
			avatars.add(i);
		}
		
		//This ACTUALLY initializes each player, one at a time
		for(int i = 0; i < players.size(); ++i) {
			Player p = players.get(i);
			
			//Get hat from pool, set hat
			Hat h = hats.remove(rand.nextInt(hats.size()));
			p.setHat(h);
			//Sheriff gets extra hp
			if(h.equals(Hat.PHD)) {
				//phd gets an extra health! wow good for them
				p.setMaxHealth(p.getMaxHealth()+1);
				p.setHealth(p.getMaxHealth());
				//Sheriff starts game
				currentPlayer = p;
			}
			
			//Set card for hat
			p.equipCard(new Card(h.getCardID()));
			
			//Set avatar card
			int avatarCardID = avatars.remove(rand.nextInt(avatars.size()));
			p.equipCard(new Card(avatarCardID));
			
			//Set who is guest and who is not
			if(p.getUsername().startsWith("anon")) {
				//P is a guest
				p.setIsGuest(true);
			} else {
				//P is not a guest
				p.setIsGuest(false);
			}
			
			//Give players cards
			for(int j = 0; j < p.getMaxHealth(); ++j) {
				if(Card.NumCardTypes < 0) {
					Card.countNumCardTypes();
				}
				int newCardID = rand.nextInt(Card.NumCardTypes);
				Card c = new Card(newCardID);
				p.getHand().add(c);
			}
			this.players.add(p);
		}
	}

	//Get the list of players
	public List<Player> getPlayers() {
		return players;
	}

	//Add player
	public boolean addPlayer(Player p) {
		return players.add(p);
	}

	//Get a player
	public Player getPlayer(String name) {
		for (Player p : players) {
			if (p.getUsername().equals(name)) {
				return p;
			}
		}
		return null;
	}

	//Get the list of alive players
	public List<Player> getAlivePlayers() {
		List<Player> alive = new ArrayList<Player>();
		for (Player p : players) {
			if (p.isAlive()) {
				alive.add(p);
			}
		}
		return alive;
	}

	//Get list of dead players
	public List<Player> getDeadPlayers() {
		List<Player> alive = new ArrayList<Player>();
		for (Player p : players) {
			if (!p.isAlive()) {
				alive.add(p);
			}
		}
		return alive;
	}

	//Add a table card
	public boolean addTableCard(Card card) {
		return tableCards.add(card);
	}

	//Remove card from table
	public boolean removeCardFromTable(Card card) {
		return tableCards.remove(card);
	}

	//Get a list of the discard pile
	public List<Card> getTableCards() {
		return tableCards;
	}

	//Get the last played card
	public Card getLastPlayedCard() {
		return tableCards.get(tableCards.size() - 1);
	}

	//Add a card to the table
	public boolean addToTable(Card card) {
		return tableCards.add(card);
	}

	//Check if game is over
	public boolean isGameOver() {
		
		// if everyone is on one team, game is over
		if (Player.sameTeam(this.getAlivePlayers())) {
			return true;
		}
		// if sheriff is dead, game is over
		if(Player.sheriffIsDead(this.getAlivePlayers())) {
			return true;
		}
		return false;
	}
	
	//Set winning hat
	public void setWinningHat() {
		if(!Player.sheriffIsDead(getAlivePlayers())) {
			winningHat = new Hat(Hat.WHITE);
		} else if(this.getAlivePlayers().size() == 1) {
			winningHat = this.getAlivePlayers().get(0).getHat();
		} else {
			winningHat = new Hat(Hat.BLACK);
		}
	}

	//Get a list of the winners
	public List<Player> getWinners() {
		setWinningHat();
		if (winningHat == null)
			return null;
		if(winningHat.equals(Hat.none))
			return null;
		List<Player> winners = new ArrayList<Player>();
		for (Player p : players) {
			if (p.getHat().sameTeam(winningHat)) {
				winners.add(p);
			}
		}
		return winners;
	}

	//Get a list of the losers
	public List<Player> getLosers() {
		setWinningHat();
		if (winningHat == null)
			return null;
		if(winningHat.equals(Hat.none))
			return null;
		List<Player> losers = new ArrayList<Player>();
		for (Player p : players) {
			if (!p.getHat().sameTeam(winningHat)) {
				losers.add(p);
			}
		}
		return losers;
	}
	
	//toString function for GameState
	public String toString() {
		String s = "";
		String endl = System.getProperty("line.separator");
		s += "GameState:" + endl;
		s += "Players: ";
		for(Player p : players){
			s += System.getProperty("line.separator");
			s += "\t";
			if(p == null) {
				s += "player is null.";
			} else {
				System.out.println(p.getUsername() + "is not null");
				s += p.toString();
			}
		}
		s += endl;
		s += "Last Played Card: " + this.getLastPlayedCard().getTitle() + endl;
		s += "Game is ";
		if(this.isGameOver()) { // game is over
			if(this.getWinners() == null) {
				s += "over, and there are no winners!?";
			} else {
				s += "over, and " + this.getWinners().get(0).getHat().toString() + " hat won";
			}
		} else { // game not over
			s += "is NOT over.";
		}
		return s;
	}
	
	//This is called after game manager increments to the next person
	public void setCurrentPlayer(Player player) {
		currentPlayer = player;
	}
	
	//Gets the current player
	public Player getCurrentPlayer() {
		return currentPlayer;
	}
	
	//Checks if it's thsi person's turn
	public boolean isTurn(String username) {
		if(username.equals(currentPlayer.getUsername())) {
			return true;
		} else {
			return false;
		}
	}
}
