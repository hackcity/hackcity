package messages;
import java.io.Serializable;

//GetHackedMessage is sent when Hack card is sent
public class GetHackedMessage implements Serializable {
	
	private static final long serialVersionUID = -8888348351955908614L;
	private String target;
	
	//Constructor
	public GetHackedMessage(String target) {
		this.target = target;
	}
	
	//Get target of the Hack
	public String getTarget() {
		return target;
	}
}
