package messages;
import java.io.Serializable;

//InvalidMoveMessage is sent when the player attempts to make a move that is not allowed
public class InvalidMoveMessage implements Serializable {
	private static final long serialVersionUID = 539079194956192711L;
	private String cardTitle, target;

	//Constructor
	public InvalidMoveMessage(String cardTitle, String target) {
		this.cardTitle = cardTitle;
		this.target = target;
	}
	
	//Get the title of the invalid card
	public String getCardTitle() {
		return cardTitle;
	}

	//Set the title of the card
	public void setCardTitle(String cardTitle) {
		this.cardTitle = cardTitle;
	}

	//Get the target of the invalid move
	public String getTarget() {
		return target;
	}

	//Set the target of the invalid move
	public void setTarget(String target) {
		this.target = target;
	}
}
