package messages;
import java.io.Serializable;

//Authentication message checks username and password of users on server side. Status of message on 
//client side notifies them on the status (error/success)
public class AuthenticationMessage implements Serializable{ 
	private static final long serialVersionUID = 1L;
	private String username, password;
	private boolean success;
	private int ID; // 0 for login, 1 for signup
	private int errorType;

	//Constructor
	public AuthenticationMessage(String username, String password, int ID, int errorType){
		this.username = username;
		this.password = password;
		this.errorType = errorType;
		this.setID(ID);
		setSuccess(false);
	}
	
	//Get username
	public String getUsername() {
		return username;
	}

	//Set username
	public void setUsername(String username) {
		this.username = username;
	}

	//Get password
	public String getPassword() {
		return password;
	}

	//Set password
	public void setPassword(String password) {
		this.password = password;
	}

	//Get success of login/signup
	public boolean getSuccess() {
		return success;
	}

	//Set success of login/signup
	public void setSuccess(boolean success) {
		this.success = success;
	}

	//Get ID (type) of message (login/signup)
	public int getID() {
		return ID;
	}

	//Set ID (type) of message (login/signup)
	public void setID(int iD) {
		ID = iD;
	}
	
	//Set error type (multiple players, login fail attempt)
	public void setErrorType(int type) {
		this.errorType = type;
	}
	
	//Get error type
	public int getErrorType() {
		return errorType;
	}
}
