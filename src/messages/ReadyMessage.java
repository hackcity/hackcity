package messages;
import java.io.Serializable;

//ReadyMessages are sent when players indicate they're ready through pressing ready button
public class ReadyMessage implements Serializable{ 
	private static final long serialVersionUID = 1L;
	private String username;

	//Constructor
	public ReadyMessage(String username){
		this.username = username;
	}
	
	//Gets username
	public String getUsername() {
		return username;
	}

	//Sets username
	public void setUsername(String username) {
		this.username = username;
	}
}
