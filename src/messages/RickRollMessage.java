package messages;
import java.io.Serializable;

//RickRollMessages are sent when RickRoll cards are played
public class RickRollMessage implements Serializable{
	private static final long serialVersionUID = 7565840308222043832L;
	private String target;
	
	//Constructor
	public RickRollMessage(String target) {
		this.target = target;
	}
	
	//Gets the target of RickRoll
	public String getTarget() {
		return target;
	}
}
