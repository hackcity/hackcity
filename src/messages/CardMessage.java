package messages;
import java.io.Serializable;

import dataTypes.Card;

//CardMessages are used to communicate effects of cards
public class CardMessage implements Serializable {

	private static final long serialVersionUID = -6179643695955343067L;
	private Card card;
	private String caster, target;
	
	//Constructor
	public CardMessage(Card card, String caster, String target) {
		this.card = card;
		this.caster = caster;
		this.target = target;
	}
	
	//Sets the card
	public void setCard(Card card) {
		this.card = card;
	}

	//Set the caster of the card
	public void setCaster(String caster) {
		this.caster = caster;
	}

	//Set the target of the card
	public void setTarget(String target) {
		this.target = target;
	}

	//Get the card
	public Card getCard() {
		return card;
	}
	
	//Get the caster of the card
	public String getCaster() {
		return caster;
	}
	
	//Get the target of the card
	public String getTarget() {
		return target;
	}
}
