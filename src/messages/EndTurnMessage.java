package messages;
import java.io.Serializable;

import dataTypes.Player;

//EndTurnMessages are sent when users press "End Turn" and it is their turn
public class EndTurnMessage implements Serializable{
	private static final long serialVersionUID = 1909935157346077171L;
	private Player player;

	//Constructor
	public EndTurnMessage(Player player) {
		this.player = player;
	}
	
	//Get the player
	public Player getPlayer() {
		return player;
	}

	//Set the player
	public void setPlayer(Player player) {
		this.player = player;
	}
}
