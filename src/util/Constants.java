package util;

import java.io.Serializable;

public class Constants implements Serializable {
	private static final long serialVersionUID = 6384675797878892291L;
	private Constants() {} // Cannot be constructed
	
	//Formats
	public static final String png = ".png";
	
	//Set the text of button
	public static final String MUTE = "<mute>";
	public static final String UNMUTE = "<unmute>";
	
	//Status of AuthenticationMessage ID
	public static final int LOGIN = 0;
	public static final int SIGNUP = 1;
	
	//Status of AuthenticationMessage errorType
	public static final int SUCCESS = -1;
	public static final int CLIENT = -1;
	public static final int IN_PLAY = 0;
	public static final int ATTEMPT_FAIL = 1;
	
	//Max number players
	public static final int MAX_PLAYERS = 8;
}
