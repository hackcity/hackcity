package util;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

//This class is in charge of interfacing with the database. 
public class DatabaseTalker {

	private Connection conn;
	
	//Constructor
	public DatabaseTalker() {
		conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/HackCity?user=root&password=root");
		} catch (ClassNotFoundException cnfe) {
			System.out.println(this.toString()+" - cnfe: "+cnfe.getMessage());
		} catch (SQLException sqle) {
			System.out.println(this.toString()+" - sqle: "+sqle.getMessage());
		}
	}
	
	//Login with username and password
	public boolean login(String username, String password) {
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM Users WHERE username=? AND pass=?");
			ps.setString(1, username);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			int count = 0;
			while(rs.next()) count++;
			ps.close();
			rs.close();
			if(count == 1) { // username with correct password found
				return true; // if login successful
			} else {
				return false;
			}
		} catch (SQLException sqle) {
			System.out.println("Database Talker: sqle: "+sqle.getMessage());
			return false;
		}
	}
	
	//Create a username with password
	public boolean createUser(String username, String password) {
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM Users WHERE username=?");
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			int count = 0;
			while(rs.next()) count++;
			ps.close();
			rs.close();
			if(count == 0) { // username not taken
				ps = conn.prepareStatement("INSERT INTO Users (username, pass) VALUES (?,?)");
				ps.setString(1, username);
				ps.setString(2, password);
				ps.executeUpdate();
				ps.close();
				ps = conn.prepareStatement("INSERT INTO GameStatistics (username, gamesPlayed, gamesWon, gamesLost, gamesAsWhiteHat, gamesAsBlackHat, gamesAsGreyHat) VALUES (?, 0, 0, 0, 0, 0, 0)");
				ps.setString(1, username);
				ps.executeUpdate();
				ps.close();
				return true; // if sign up successful
			} else { // username already exists
				return false;
			}
		} catch (SQLException sqle) {
			System.out.println(this.toString()+" - sqle: "+sqle.getMessage());
			return false;
		}
	}

	//Get statistics at end of game
	public ArrayList<Tuple> getStatistics(String username) {
		ArrayList<Tuple> stats = new ArrayList<Tuple>();
		try {
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM GameStatistics WHERE username=?");
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				stats.add(new Tuple("gamesPlayed", rs.getInt("gamesPlayed")));
				stats.add(new Tuple("gamesWon", rs.getInt("gamesWon")));
				stats.add(new Tuple("gamesLost", rs.getInt("gamesLost")));
				stats.add(new Tuple("gamesAsWhiteHat", rs.getInt("gamesAsWhiteHat")));
				stats.add(new Tuple("gamesAsBlackHat", rs.getInt("gamesAsBlackHat")));
			    stats.add(new Tuple("gamesAsGreyHat", rs.getInt("gamesAsGreyHat")));
			}
		} catch (SQLException sqle) {
			System.out.println(this.toString()+" - sqle: "+sqle.getMessage());
		}
		return stats;
	}
	
	//Update the games played of that user
	public boolean updateGamesPlayed(String username) {
		try {
			PreparedStatement ps = conn.prepareStatement("UPDATE GameStatistics SET gamesPlayed = gamesPlayed + 1 WHERE username=?");
			ps.setString(1, username);
			ps.executeUpdate();
			ps.close();
			return true;
		} catch (SQLException sqle) {
			System.out.println(this.toString()+" - sqle: "+sqle.getMessage());
			return false;
		}
	}

	//Update the games won of that user
	public boolean updateGamesWon(String username) {
		try {
			PreparedStatement ps = conn.prepareStatement("UPDATE GameStatistics SET gamesWon = gamesWon + 1 WHERE username=?");
			ps.setString(1, username);
			ps.executeUpdate();
			ps.close();
			return true;
		} catch (SQLException sqle) {
			System.out.println(this.toString()+" - sqle: "+sqle.getMessage());
			return false;
		}
	}
	
	//Update games lost of that user
	public boolean updateGamesLost(String username) {
		try {
			PreparedStatement ps = conn.prepareStatement("UPDATE GameStatistics SET gamesLost = gamesLost + 1 WHERE username=?");
			ps.setString(1, username);
			ps.executeUpdate();
			ps.close();
			return true;
		} catch (SQLException sqle) {
			System.out.println(this.toString()+" - sqle: "+sqle.getMessage());
			return false;
		}
	}
	
	//Update games won as white hat
	public boolean updateGamesAsWhiteHat(String username) {
		try {
			PreparedStatement ps = conn.prepareStatement("UPDATE GameStatistics SET gamesAsWhiteHat = gamesAsWhiteHat + 1 WHERE username=?");
			ps.setString(1, username);
			ps.executeUpdate();
			ps.close();
			return true;
		} catch (SQLException sqle) {
			System.out.println(this.toString()+" - sqle: "+sqle.getMessage());
			return false;
		}
	}
	
	//Update games won as grey hat
	public boolean updateGamesAsGreyHat(String username) {
		try {
			PreparedStatement ps = conn.prepareStatement("UPDATE GameStatistics SET gamesAsGreyHat = gamesAsGreyHat + 1 WHERE username=?");
			ps.setString(1, username);
			ps.executeUpdate();
			ps.close();
			return true;
		} catch (SQLException sqle) {
			System.out.println(this.toString()+" - sqle: "+sqle.getMessage());
			return false;
		}
	}
	
	//Update games won as black hat
	public boolean updateGamesAsBlackHat(String username) {
		try {
			PreparedStatement ps = conn.prepareStatement("UPDATE GameStatistics SET gamesAsBlackHat = gamesAsBlackHat + 1 WHERE username=?");
			ps.setString(1, username);
			ps.executeUpdate();
			ps.close();
			return true;
		} catch (SQLException sqle) {
			System.out.println(this.toString()+" - sqle: "+sqle.getMessage());
			return false;
		}
	}
}
