package util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

//Encrypts passwords
public class Encryption {
	
	private Encryption() {} // cannot be constructed

	//Encrypts password (used online source code for this)
	//http://stackoverflow.com/questions/415953/how-can-i-generate-an-md5-hash
	public static String encrypt(String s) {
    	try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			byte[] arr = md5.digest(s.getBytes());
	        StringBuffer sbuffer = new StringBuffer();
	        for (int i = 0; i < arr.length; ++i) {
	          sbuffer.append(Integer.toHexString((arr[i] & 0xFF) | 0x100).substring(1,3));
	       }
	        return sbuffer.toString();
		} catch (NoSuchAlgorithmException e) {
			System.out.println("Encryption Exception: " + e);
			return "";
		}
	}
}
