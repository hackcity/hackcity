package util;
import java.io.Serializable;

//Tuple pair
public class Tuple implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String key;
	private Integer value;
	
	//Constructor
	public Tuple(String key, Integer value) {
		this.key = key;
		this.value = value;
	}
	
	//Get key
	public String getKey() {
		return key;
	}
	//Get value
	public Integer getValue() {
		return value;
	}
}