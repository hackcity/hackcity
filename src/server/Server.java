package server;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import dataTypes.Hat;
import dataTypes.Player;
import util.DatabaseTalker;
import util.Tuple;

//Server class that communicates with multiple clients
public class Server {
	
	private ServerSocket serverSocket;
	Vector<ServerThread> serverThreads;
	Vector<Player> playerList; // TODO I ADDED THIS
	Vector<String> playerUsernames;
	GameManager gameManager;
	private boolean gameAlreadyOver;
	
	//Constructor
	public Server(int port) {
		//Initialize variables
		serverSocket = null;
		serverThreads = new Vector<ServerThread>();
		playerList = new Vector<Player>();
		playerUsernames = new Vector<String>();
		gameManager = new GameManager(this);
		gameAlreadyOver = false;
		
		Enumeration<NetworkInterface> n = null;
		try {
			n = NetworkInterface.getNetworkInterfaces();
		    while(n.hasMoreElements())
		    {
		        Enumeration<InetAddress> a = n.nextElement().getInetAddresses();
		        while(a.hasMoreElements())
		        {            
		            String ipAddress = a.nextElement().getHostAddress().toString();
		            if(ipAddress.contains("10.")){
		            System.out.println("Current Address: " + ipAddress);
		            }
		        }
		    }
		} catch (SocketException e1) {
			e1.printStackTrace();
		}
		
		//Create a server socket
		try {
			serverSocket = new ServerSocket(port);
			while(true) {
				System.out.println("Waiting for connections...");
				//Connection with client
				Socket socket = serverSocket.accept();
				System.out.println("Connection from " + socket.getInetAddress());
				//Make a new thread
				ServerThread thread = new ServerThread(socket, this);
				serverThreads.add(thread);
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(serverSocket != null) {
				try {
					serverSocket.close();
				} catch (IOException ioe) {
					System.out.println("IOE: closing server socket " + ioe.getMessage());
				}
			}
		}
	}
	
	//Server print message
	public void print(String s){
		System.out.println(s);
	}
	
	//Remove thread
	public void removeServerThread(ServerThread thread) {
		serverThreads.remove(thread);
	}
	
	//Add a player
	public void addPlayer(Player p){
		playerList.add(p);
	}
	
	//Send player to all clients
	public void sendPlayerToAllClients(Player p){ 
		for(ServerThread st: serverThreads) {
			st.sendPlayer(p);
		}
	}
	
	//Get player list
	public Vector<Player> getPlayerList(){
		return playerList;
	}
	
	//Send game state to all clients
	public void sendGameStateToAllClients() {
		for(ServerThread st : serverThreads) {
			st.sendGameState(gameManager.getGameState());
		}
	}
	
	//Send statistics to all clients
	public void sendStatisticsToAllClients(DatabaseTalker db) {
		Map<String,ArrayList<Tuple>> allStats = new HashMap<String,ArrayList<Tuple>>();
		for(Player p : gameManager.getGameState().getPlayers()) {
			allStats.put(p.getUsername(), db.getStatistics(p.getUsername()));
		}
		for(ServerThread st : serverThreads) {
			System.out.println("sending all stats to serverthread");
			st.sendStatistics(allStats);
		}
	}
	
	//Send hacked message to all clients
	public void sendGetHackedMessageToAllClients(String target){
		for(ServerThread st : serverThreads) {
			st.sendGetHackedMessage(target);
		}
	}
	
	//Send a rickroll message
	public void sendRickRollMessageToAllClients(String target) {
		for(ServerThread st : serverThreads) {
			st.sendRickRollMessage(target);
		}
	}
	
	//Get the game manager
	public GameManager getGameManager() {
		return gameManager;
	}
	
	//Start the game
	public void startGame(){
		//initialize game state and send to everyone
		gameManager.initalizeGameState();
		//System.out.println(gameManager.getGameState());
		sendGameStateToAllClients();
	}
	
	//Get player usernames
	public Vector<String> getPlayerUsernames() {
		return playerUsernames;
	}
	
	//Add username
	public void addUsername(String username) {
		playerUsernames.add(username);
	}
	
	//Check if username exists
	public boolean usernameExists(String username) {
		
		for(String i : playerUsernames) {
			if (username.equals(i))
				return true;
		}
		return false;
	}
	
	//Set game over state
	public void setGameIsOver() {
		if(!gameAlreadyOver) {
			gameAlreadyOver = true;
			DatabaseTalker dt = new DatabaseTalker();
			for(Player p : gameManager.getGameState().getWinners()) {
				System.out.println("winner = "+p.getUsername());
				dt.updateGamesWon(p.getUsername());
				dt.updateGamesPlayed(p.getUsername());
				if(p.getHat().equals(Hat.WHITE)) dt.updateGamesAsWhiteHat(p.getUsername());
				else if(p.getHat().equals(Hat.GREY)) dt.updateGamesAsGreyHat(p.getUsername());
				else if(p.getHat().equals(Hat.BLACK)) dt.updateGamesAsBlackHat(p.getUsername());
				else if(p.getHat().equals(Hat.PHD)) dt.updateGamesAsWhiteHat(p.getUsername());
			}
			for(Player p : gameManager.getGameState().getLosers()) {
				System.out.println("loser = "+p.getUsername());
				dt.updateGamesLost(p.getUsername());
				dt.updateGamesPlayed(p.getUsername());
				if(p.getHat().equals(Hat.WHITE)) dt.updateGamesAsWhiteHat(p.getUsername());
				else if(p.getHat().equals(Hat.GREY)) dt.updateGamesAsGreyHat(p.getUsername());
				else if(p.getHat().equals(Hat.BLACK)) dt.updateGamesAsBlackHat(p.getUsername());
				else if(p.getHat().equals(Hat.PHD)) dt.updateGamesAsWhiteHat(p.getUsername());
			}
		}
	}
	
	// This is the main function. You use this to start. 
	// Everything begins here. 
	// This function is where things begin, and start.
	// Use this function to start things and/or/xor/nand begin things. 
	// Do not delete this function because it is the beginning and the start.
	public static void main(String[] args) {
		new Server(6789);
	}
}