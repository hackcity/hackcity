package server;
import java.util.Random;

import dataTypes.Card;
import dataTypes.GameState;
import dataTypes.Player;
import messages.CardMessage;

//GameManager that controls game states
public class GameManager {
	GameState gameState;
	Card lastCard;
	String lastCaster, lastTarget;
	Server server;
	boolean hackAlreadyPlayed;
	Random rand;
	
	//Constructor
	public GameManager(Server server) {
		gameState = null;
		this.server = server;
		hackAlreadyPlayed = false;
		rand = new Random();
	}
	
	//Get the game state
	public GameState getGameState(){
		return gameState;
	}
	
	//Initialize the game state
	public void initalizeGameState() {
		gameState = new GameState(server.getPlayerList());
	}

	//Increments to next turn after a player finishes
	public void incrementTurn(Player finishedPlayer) { // this is the correct version
		int turn = -1;
		//Find the player index
		for(int i = 0; i < gameState.getAlivePlayers().size(); i++) {
			if(finishedPlayer.equals(gameState.getAlivePlayers().get(i))) {
				turn = i;
				break;
			}
		}
		//If at the end of the player list, moves to beginning
		if(turn == (gameState.getAlivePlayers().size() - 1))
			turn = 0;
		else
			turn++;
		
		//Set it back to false once move on to someone else's turn
		Player nextPlayer = gameState.getAlivePlayers().get(turn);
		gameState.setCurrentPlayer(nextPlayer);
		hackAlreadyPlayed = false; 

		//Draw two cards each turn
		//Delete cards if needed
		nextPlayer.drawRandomCard();
		nextPlayer.drawRandomCard();
	}
	
	//Check if valid move
	public boolean isValidMove(CardMessage cardMessage) {
		//Have card, caster, and target
		Card card = cardMessage.getCard();
		String caster = cardMessage.getCaster();
		String target = cardMessage.getTarget();
		Player casterPlayer = gameState.getPlayer(caster);	
		
		if(!casterPlayer.isAlive()) return false;
		
		switch(card.getTitle()) {
		case "Hack": 
			//If it's not the caster's turn, they can't play a hack card
			if(!gameState.isTurn(caster)) return false;
			//Caster can't play more than one hack a turn
			if(hackAlreadyPlayed) return false; 
			//Can't hack yourself
			if(caster.equals(target)) return false; 
			break;
		case "CounterHack":
		case "VPN":
			//The last played card must have been a hack
			if(!gameState.getLastPlayedCard().getTitle().equals("Hack")) return false;
			//Last target must have been the caster
			if(!lastTarget.equals(caster)) return false;
			break;
		case "Red Cow":
			//If it's not the caster's turn, they can't play a red cow card
			if(!gameState.isTurn(caster)) return false;
			//Can only play red cow on yourself
			if(!caster.equals(target)) return false;
			//Can't use if health is at max
			if(casterPlayer.getHealth() == casterPlayer.getMaxHealth()) return false;
			break;
		//No ddos case needed because has no conditions
		}
		
		return true; //If got down here everything is ok
	}
	
	//Process a card
	public GameState process(Card card, String caster, String target) {
		switch(card.getTitle()) {
		case "Hack":
			Bang(card, caster, target);
			break;
		case "CounterHack":
			CounterHack(card, caster, target);
			break;
		case "VPN":
			Miss(card, caster, target);
			break;
		case "Red Cow":
			Beer(card, caster, target);
			break;
		case "DDOS":
			DDOS(card, caster, target);
			break;
		case "Rick Roll":
			rickRoll(card, caster, target);
			break;
		}
		lastCard = card;
		gameState.addTableCard(card);
		lastCaster = caster;
		lastTarget = target;
		return this.gameState;
	}
		
	//Bang card
	private void Bang(Card card, String caster, String target) {
		gameState.getPlayer(caster).removeCardFromHand(card);
		gameState.getPlayer(target).changeHealth(-1);
		hackAlreadyPlayed = true;
	}
	
	//CounterHack card
	private void CounterHack(Card card, String caster, String target) {
		gameState.getPlayer(caster).removeCardFromHand(card);
		gameState.getPlayer(lastCaster).changeHealth(-1);
	}
	
	//Miss card
	private void Miss(Card card, String caster, String target) {
		gameState.getPlayer(caster).removeCardFromHand(card);
		gameState.getPlayer(caster).changeHealth(1);
	}
	
	//Beer card (get a life)
	private void Beer(Card card, String caster, String target) {
		// HEAL THE DAMAGE
		gameState.getPlayer(caster).removeCardFromHand(card);
		gameState.getPlayer(caster).changeHealth(1);
	}
	
	//DDOS card (flip screens)
	private void DDOS(Card card, String caster, String target) {
		// GET DDOS'D
		gameState.getPlayer(caster).removeCardFromHand(card);
		server.sendGetHackedMessageToAllClients(target);
	}
	
	//Rickroll card (bring up rickroll on browser)
	private void rickRoll(Card card, String caster, String target) {
		// GET RICK ROLLED
		gameState.getPlayer(caster).removeCardFromHand(card);
		server.sendRickRollMessageToAllClients(target);
	}
}
