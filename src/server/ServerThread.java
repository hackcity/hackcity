package server;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Map;

import dataTypes.GameState;
import dataTypes.Player;
import messages.AuthenticationMessage;
import messages.CardMessage;
import messages.EndTurnMessage;
import messages.GetHackedMessage;
import messages.InvalidMoveMessage;
import messages.ReadyMessage;
import messages.RickRollMessage;
import util.Constants;
import util.DatabaseTalker;
import util.Tuple;

//ServerThreads created by server
public class ServerThread extends Thread {
	private Server server;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	
	//Constructor
	public ServerThread(Socket socket, Server server) {
		try {
			this.server = server;
			oos = new ObjectOutputStream(socket.getOutputStream());
			ois = new ObjectInputStream(socket.getInputStream());
			//Start the thread
			this.start();
		} catch (IOException ioe) {
			//Thrown when no connection with the program on the other side (not be able to read or write)
			System.out.println("ioe: " + ioe.getMessage());
		}
	}
	
	//Run a server thread
	public void run() {
		try { 
			while(true) {
				Object message = (Object) ois.readObject();
				DatabaseTalker dt = new DatabaseTalker(); // moved this outside
				//Login and signup case
				if(message instanceof AuthenticationMessage){ 
					AuthenticationMessage authenticationMessage = (AuthenticationMessage) message;
					//Login case
					if(authenticationMessage.getID() == 0){
						//Check username with password
						if(dt.login(authenticationMessage.getUsername(), authenticationMessage.getPassword())){
							//Error - If user is already logged in game
							if(server.usernameExists(authenticationMessage.getUsername())) {
								sendAuthenticationMessage(authenticationMessage.getUsername(),"fail", Constants.LOGIN, Constants.IN_PLAY);
								server.print("Login Attempt: " + authenticationMessage.getUsername() + "| FAIL");
							}
							//Success
							else {
								//Add the username
								server.addUsername(authenticationMessage.getUsername());
								sendAuthenticationMessage(authenticationMessage.getUsername(),"success", Constants.LOGIN, Constants.SUCCESS);
								server.print("Login Attempt: " + authenticationMessage.getUsername() + "| SUCCESS");
							}
						} 
						//User doesn't exist
						else {
							System.out.println("Server: Couldn't login ATTEMPTFAIL");
							sendAuthenticationMessage(authenticationMessage.getUsername(),"fail", Constants.LOGIN, 1);
							server.print("Login Attempt: " + authenticationMessage.getUsername() + "| FAIL");
						}
					} 
					//Signup case
					else {
						//Signup success
						if(dt.createUser(authenticationMessage.getUsername(), authenticationMessage.getPassword())){
							sendAuthenticationMessage(authenticationMessage.getUsername(),"success",Constants.SIGNUP, Constants.SUCCESS);
							server.print("Create Account Attempt: " + authenticationMessage.getUsername() + "| SUCCESS");
						} 
						//Signup fail
						else {
							sendAuthenticationMessage(authenticationMessage.getUsername(),"fail",Constants.SIGNUP, Constants.ATTEMPT_FAIL);
							server.print("Create Account Attempt: " + authenticationMessage.getUsername() + "| FAIL");
						}
					}	
				} else if(message instanceof ReadyMessage) { 
					Player p = new Player(((ReadyMessage)message).getUsername());
					server.addPlayer(p);
					server.sendPlayerToAllClients(p);
					server.print(((ReadyMessage) message).getUsername()+ " is ready to play");
				} else if(message instanceof String){
					if(((String)message).contains("PlayerListRequest")){
						for(Player p: server.getPlayerList()){
							sendPlayer(p);
						}
					} else if(((String)message).equals("getGameState")) { // TODO no longer need?
						server.sendGameStateToAllClients(); 
					} else if(((String)message).startsWith("getStats")) {
						server.sendStatisticsToAllClients(dt);
					} else if(((String)message).startsWith("RequestGameStart")) {
						server.startGame();
					} else if(((String)message).contains("Guest Login: ")){
						server.print((String)message);
					} else if(((String)message).equals("game is over")) {
						server.setGameIsOver();
					}
				} else if(message instanceof CardMessage) {
					GameManager gm = server.getGameManager();
					CardMessage cardMessage = (CardMessage) message;
					if(gm.isValidMove(cardMessage)) {
						gm.process(cardMessage.getCard(), cardMessage.getCaster(), cardMessage.getTarget());
						server.sendGameStateToAllClients();
					} else{
						sendInvalidMoveMessage(cardMessage.getCard().getTitle(), cardMessage.getTarget());
					}
				} else if(message instanceof EndTurnMessage) {
					EndTurnMessage etm = (EndTurnMessage) message;
					//increment player based on player that just finished
					server.getGameManager().incrementTurn(etm.getPlayer());
					server.sendGameStateToAllClients();
				} 
			}
		} catch (ClassNotFoundException cnfe) {
			System.out.println("ServerThread cnfe: " + cnfe.getMessage());
		} catch (IOException ioe) { 
			System.out.println("ServerThread ioe: " + ioe.getMessage());
		} finally {
			server.removeServerThread(this);
		}
	}
	
	//Send a player
	public void sendPlayer(Player p){
		try { 
			oos.reset();
			oos.writeObject(p);
			oos.flush();
		} catch(IOException ioe) {
			System.out.println("ServerThread sending player ioe: " + ioe.getMessage());
		}
	}
	
	//Send a Message to the client
	public void sendAuthenticationMessage(String user, String pass, int ID, int errorType){
		try { 
			oos.reset();
			oos.writeObject(new AuthenticationMessage(user, pass, ID, errorType));
			oos.flush();
		} catch(IOException ioe) {
			System.out.println("ServerThread sendAuthentication Message ioe: " + ioe.getMessage());
		}
	}
	
	//Send game state
	public void sendGameState(GameState gs) {
		try {
			oos.reset();
			oos.writeObject(gs);
			oos.flush();
		} catch(IOException ioe) {
			System.out.println("ServerThread sending gamestate ioe: "+ioe.getMessage());
		}
	}
	
	//Send statistics
	public void sendStatistics(Map<String,ArrayList<Tuple>> allStats) {
		try {
			oos.reset();
			oos.writeObject(allStats); 
			oos.flush();
		} catch(IOException ioe) {
			System.out.println("ServerThread sending all stats ioe: "+ioe.getMessage());
		}
	}
	
	//Send GetHacked message
	public void sendGetHackedMessage(String target){
		try {
			oos.reset();
			oos.writeObject(new GetHackedMessage(target));
			oos.flush();
		} catch(IOException ioe) {
			System.out.println("ServerThread sending getHackedMessage ioe: "+ioe.getMessage());
		}
	}
	
	//Send rickroll message
	public void sendRickRollMessage(String target) {
		try {
			oos.reset();
			oos.writeObject(new RickRollMessage(target));
			oos.flush();
		} catch(IOException ioe) {
			System.out.println("ServerThread sending RickRollMessage ioe: "+ioe.getMessage());
		}
	}
	
	//Send invalid move message
	public void sendInvalidMoveMessage(String cardTitle, String target) {
		try {
			oos.reset();
			oos.writeObject(new InvalidMoveMessage(cardTitle, target));
			oos.flush();
		} catch(IOException ioe) {
			System.out.println("ServerThread sending invalidmovemessage ioe: "+ioe.getMessage());
		}
	}

}