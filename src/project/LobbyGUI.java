package project;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

import dataTypes.Player;

//Lobby GUI
public class LobbyGUI {
	
	private MainFrameGUI main;
	private JPanel basePanel, titlePanel, southPanel;
	private JLabel titleLabel, ipLabel, portLabel, connectedLabel;
	private JButton startButton, readyButton;
	private JPanel grid;
	private SoundButton soundButton;
	
	//Constructor for lobby
	public LobbyGUI(MainFrameGUI main){
		this.main = main;
		//Set the UI
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			UIManager.getLookAndFeelDefaults().put("Label.font", new Font("Courier New", Font.PLAIN, 18));
			UIManager.getLookAndFeelDefaults().put("Label.foreground", new Color(0,255,0));
			UIManager.getLookAndFeelDefaults().put("Button.font", new Font("Courier New", Font.PLAIN, 16));
			UIManager.getLookAndFeelDefaults().put("Button.foreground", new Color(0,255,0));
			UIManager.getLookAndFeelDefaults().put("Button.background", new Color(50,50,50));
			UIManager.getLookAndFeelDefaults().put("Button.border", new Color(50,50,50));
			UIManager.getLookAndFeelDefaults().put("Panel.background", new Color(0,0,0));
		} catch (Exception e) {
			e.printStackTrace();
		}
		initializeLoginVariables();
		createGUI();
		addActionAdapters();
	}
	
	//Initialize variables
	private void initializeLoginVariables(){
		titleLabel = new JLabel("<Waiting Room>");
		titleLabel.setFont(new Font("Courier New", Font.PLAIN, 40));
		portLabel = new JLabel("     ");
		ipLabel = new JLabel("     ");
		startButton = new JButton("this.start()");
		startButton.setMaximumSize(new Dimension(330,30));
		startButton.setEnabled(false);
		readyButton = new JButton("<ready>");
		readyButton.setMaximumSize(new Dimension(330,30));
		soundButton = new SoundButton(main.getSoundManager());
		soundButton.setMaximumSize(new Dimension(330,30));
		connectedLabel = new JLabel("     <Connected Players>");
		connectedLabel.setFont(new Font("Courier New", Font.BOLD, 20));
		basePanel = new JPanel();
		southPanel = new JPanel();
		titlePanel = new JPanel();
	}
	
	//Create the GUI
	private void createGUI(){
		JPanel smalltitlePanel = new JPanel();
		smalltitlePanel.setLayout(new BoxLayout(smalltitlePanel, BoxLayout.Y_AXIS));
		smalltitlePanel.add(titleLabel);
		smalltitlePanel.add(ipLabel);
		smalltitlePanel.add(portLabel);
		smalltitlePanel.add(new JLabel(" "));
		smalltitlePanel.add(connectedLabel);
		smalltitlePanel.setMaximumSize(smalltitlePanel.getPreferredSize());
		titlePanel.add(smalltitlePanel);
		
		grid = new JPanel();
		GridLayout gl = new GridLayout(4,2);
		gl.setHgap(100);
		gl.setVgap(10);
		grid.setLayout(gl);
		
		basePanel.add(grid);
		
		southPanel.setLayout(new BoxLayout(southPanel,BoxLayout.Y_AXIS));
		JPanel readyPanel = new JPanel();
		readyButton.setPreferredSize(new Dimension(330,25));
		readyPanel.add(readyButton);
		southPanel.add(readyPanel);
		
		JPanel startPanel = new JPanel();
		startButton.setPreferredSize(new Dimension(330,25));
		startPanel.add(startButton);
		southPanel.add(startPanel);
		
		JPanel soundPanel = new JPanel();
		soundButton.setFont( new Font("Courier New", Font.PLAIN, 12));
		soundButton.setPreferredSize(new Dimension(70,20));
		soundPanel.add(soundButton);
		soundPanel.setBackground(Color.black);
		southPanel.add(soundPanel);
	}
	
	//Add actions
	private void addActionAdapters(){
		//Start button (attempts to start game)
		startButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent arg0) {
		    	main.playSound("click");
		    	if(main.getPlayerListSize()>=4){ // can start game
		    		main.playSound("click");
		    		main.sendMessage("RequestGameStart");
		    		//Start game
		    	} else{
		    		//Play error sound
		    		JOptionPane.showMessageDialog(null, "Not Enough Players", "ERROR", JOptionPane.ERROR_MESSAGE);
		    	}
		    }
		});
		
		//Ready button indicates ready status
		readyButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent arg0) {
		    	main.playSound("click");
		    	readyButton.setEnabled(false); 
		    	if(main.getPlayerListSize()<8){ 
			    	main.sendReadyMessage();
			    	startButton.setEnabled(true);
		    	} else {
		    		readyButton.setText("JOIN FAILED // ROOM FULL");
		        	basePanel.repaint();
		        	basePanel.validate();
		    	}
		    }
		});
	}
	
	//Populate the players currently in game
	public void populatePlayers(List<Player> playerList){
		//Refreshes the GUI
		grid.removeAll();
		for(Player p : playerList) {
			grid.add(new JLabel(p.getUsername()));
		}
    	basePanel.repaint();
    	basePanel.validate();
	}
	
	//Add a player to the lobby
	public void addPlayerToLobby(String name){
		grid.add(new JLabel(name));
    	basePanel.repaint();
    	basePanel.validate();
	}
	
	//Gets south panel
	public JPanel getSouthPanel(){
		return southPanel;
	}
	
	//Gets center panel
	public JPanel getCenterPanel(){
		return basePanel;
	}
	
	//Gets north panel
	public JPanel getNorthPanel(){
		return titlePanel;
	}
	
	//Get sound button
	public SoundButton getSoundButton() {
		return soundButton;
	}
}

