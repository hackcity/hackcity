package project;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

import dataTypes.Card;
import dataTypes.GameState;
import dataTypes.Player;
import util.GUIScrambler;

//Main GUI shown to user runs program
public class MainFrameGUI extends JFrame {

	private static final long serialVersionUID = 1L;
	Client client;
	private CreateAccountGUI caGUI;
	private LoginGUI lGUI;
	private LobbyGUI lobGUI;
	private ResultGUI rGUI;
	private GameGUI gGUI;
	private JPanel northPanel, centerPanel, southPanel;
	private SoundManager soundManager;
	private boolean first;

	//Constructor
	MainFrameGUI(Client client, SoundManager soundManager) {
		super("Hack City");
		first = true;
		this.client = client;
		this.soundManager = soundManager;
		caGUI = new CreateAccountGUI(this);
		lGUI = new LoginGUI(this);
		lobGUI = new LobbyGUI(this);
		rGUI = new ResultGUI(this);
		gGUI = new GameGUI(this);
		initializeVariables();
		createGUI();
		this.setVisible(true);
	}

	//Initialize variables
	public void initializeVariables() {
		northPanel = new JPanel();
		centerPanel = new JPanel();
		southPanel = new JPanel();
	}

	//Creates GUI
	public void createGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds((int)Math.round(screenSize.width*.1),(int)Math.round(screenSize.height*.05),550,550);//location x,y //width,height		
		addPanels();
		displayLogin();
	}

	//Displays the game GUI
	public void displayGameGUI(GameState gameState) {
		removePanels();
		Toolkit tk = Toolkit.getDefaultToolkit();
		Dimension screenSize = tk.getScreenSize();
		if(first){
			setLocation(screenSize.width/5,0);
			setSize(800, 750);
			first = false;
		}
		
		gGUI.createGameGUI(gameState);
		northPanel = gGUI.getNorthPanel();
		centerPanel = gGUI.getCenterPanel();
		southPanel = gGUI.getSouthPanel();
		addPanels();
		this.repaint();
		this.validate();
	}	
	
	//Displays the results
	public void displayResult() {
		removePanels();
		this.soundManager.startBGM("LoginBGM");
		this.soundManager.playSound("war_games_secure");
		northPanel = rGUI.getNorthPanel();
		centerPanel = rGUI.getCenterPanel();
		southPanel = rGUI.getSouthPanel();
		addPanels();
		this.repaint();
		this.validate();
	}

	//Displays loginGUI
	public void displayLogin() {
		removePanels();
		northPanel = lGUI.getNorthPanel();
		centerPanel = lGUI.getCenterPanel();
		southPanel = lGUI.getSouthPanel();
		addPanels();
		this.repaint();
		this.validate();
	}

	//Clears text fields for login
	public void clearLogin() {
		lGUI.clearTextFields();
	}

	//Displays createAccountGUI
	public void displayCreateAccount() {
		removePanels();
		if(!soundManager.isPlaying())
			caGUI.getSoundButton().setText(util.Constants.UNMUTE);
		else
			caGUI.getSoundButton().setText(util.Constants.MUTE);
		northPanel = caGUI.getNorthPanel();
		centerPanel = caGUI.getCenterPanel();
		southPanel = caGUI.getSouthPanel();
		addPanels();
		this.repaint();
		this.validate();
	}

	//Clears the text fields for createAccount
	public void clearCreateAccount() {
		caGUI.clearTextfields();
	}

	//Displays the lobby
	public void displayLobby() {
		removePanels();
		if(!soundManager.isPlaying())
			lobGUI.getSoundButton().setText(util.Constants.UNMUTE);
		else
			lobGUI.getSoundButton().setText(util.Constants.MUTE);
		northPanel = lobGUI.getNorthPanel();
		centerPanel = lobGUI.getCenterPanel();
		southPanel = lobGUI.getSouthPanel();
		addPanels();
		this.repaint();
		this.validate();
	}

	//Adds a player to the lobby
	public void addPlayerToLobby(String name) {
		lobGUI.addPlayerToLobby(name);
	}

	//Send a message to server
	public void sendMessage(String message) {
		client.sendMessage(message);
	}

	//Plays a sound
	public void playSound(String sound){
		client.playSound(sound);
	}
	
	//Adds panels
	private void addPanels(){
		add(northPanel, BorderLayout.NORTH);
		add(centerPanel, BorderLayout.CENTER);
		add(southPanel, BorderLayout.SOUTH);
	}

	//Removes panels
	private void removePanels() {
		remove(northPanel);
		remove(centerPanel);
		remove(southPanel);
	}

	//Sends message to server to verify identity of user
	public void sendAuthenticationMessage(String user, String pass, int ID, int errorType) {
		client.sendAuthenticationMessage(user, pass, ID, errorType);
	}

	//Sends message to server indicating ready status
	public void sendReadyMessage() {
		client.sendReadyMessage();
	}
	
	//Gets the result GUI
	public ResultGUI getRGUI() {
		return rGUI;
	}

	//Set username
	public void setUsername(String username){
		client.setUsername(username);
	}
	
	//Get username
	public String getUsername() {
		return client.getUsername();
	}

	//Get the current number of players
	public int getPlayerListSize(){
		return client.getPlayerListSize();
	}
	
	//Set the title of the window
	public void setTitle(){
		this.setTitle("Hack City - "+ client.getUsername());
	}
	
	//Get sound manager
	public SoundManager getSoundManager() {
		return this.soundManager;
	}
	
	//Send a cardMessage to server. Passes the card itself as well as the caster and target
	public void sendCardMessage(Card card, String caster, String target) {
		client.sendCardMessage(card, caster, target);
	}
	
	//Hacks the players screen
	public void messUpScreen() {
		GUIScrambler t = new GUIScrambler();
		this.soundManager.playSound("worms");
		t.combo();
	}
	
	//Change screen resolution
	public void changeScreenResolution() {
		GUIScrambler t = new GUIScrambler();
		this.soundManager.playSound("lasers");
		t.changeScreenResolution();
	}
	
	//Rickroll
	public void rickRoll() {
		GUIScrambler t = new GUIScrambler();
		this.soundManager.playSound("GetHacked");
		t.rickRoll();
	}
	
	//Sends an endTurnMessage
	public void sendEndTurnMessage(Player player) {
		client.sendEndTurnMessage(player);
	}
	
	//Get gameGUI
	public GameGUI getGameGUI() {
		return gGUI;
	}
}