package project;
import java.io.File;
import java.util.Vector;

// In charge of creating a new thread for each sound
public class SoundManager {

	//All the sounds that are currently being played
	private Vector<SoundThread> soundThreads;
	private SoundThread bgm;
	private boolean isPlaying;
	
	//Constructor
	public SoundManager() {
		soundThreads = new Vector<SoundThread>();
		isPlaying = true;
	}

	//Plays a sound
	public boolean playSound(String sound) {
		//Get the sound file
		File temp = new File("src/project/sounds/" + sound + ".wav");
		
		//Create a new sound and add to threads
		if (temp.exists()) {
			SoundThread st = new SoundThread(sound, this, false);
			soundThreads.add(st);
			st.start();
			return true;
		} else {
			return false;
		}
	}

	//Remove a sound thread
	public void removeThread(SoundThread st) {
		soundThreads.remove(st);
	}

	//Start the background music
	public void startBGM(String sound) {
		if (bgm != null) {
			this.stopBGM();
		}
		bgm = new SoundThread(sound, this, true);
		soundThreads.add(bgm);
		bgm.start();
	}

	//Stop the background music
	public void stopBGM() {
		bgm.endNow();
	}
	
	//Pauses the background music
	public void pauseBGM() {
		for(SoundThread st : soundThreads) {
			st.pause();
			isPlaying = false;
		}
	}
	
	//Play background music
	public void playBGM() {
		for(SoundThread st : soundThreads) {
			st.play();
			isPlaying = true;
		}
	}
	
	//Returns status of background music
	public boolean isPlaying() {
		return isPlaying;
	}
}
