package project;
import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import dataTypes.Card;
import dataTypes.GameState;
import dataTypes.Player;
import messages.AuthenticationMessage;
import messages.CardMessage;
import messages.EndTurnMessage;
import messages.GetHackedMessage;
import messages.InvalidMoveMessage;
import messages.ReadyMessage;
import messages.RickRollMessage;
import util.Constants;
import util.Tuple;

//Client class communicates with ServerThread and Server classes
public class Client extends Thread {
	
	private ObjectInputStream ois;
	private ObjectOutputStream oos;	
	private MainFrameGUI mfGUI;
	private String username;
	private SoundManager sm;
	private ArrayList<String> playerList;
	private boolean firstGameState;
	
	//Constructor
	public Client(String hostname,int port){
		firstGameState = true;
		playerList = new ArrayList<String>();
		sm = new SoundManager();
		Socket s = null;
		try {
			s = new Socket(hostname, port);
			sm.startBGM("LoginBGM");
			ois = new ObjectInputStream(s.getInputStream());
			oos = new ObjectOutputStream(s.getOutputStream());
			this.mfGUI = new MainFrameGUI(this, sm);
			this.start();
		} catch (Exception e) {
			System.out.println("Server does not exist.");
			System.exit(0);
		}
	}

	@SuppressWarnings("unchecked")
	public void run(){
		try{
			while(true) {
				//Set UI of the popups
				UIManager.getLookAndFeelDefaults().put("OptionPane.background", new Color(0,0,0));
				UIManager.getLookAndFeelDefaults().put("OptionPane.messageForeground", new Color(0,255,0));
				UIManager.getLookAndFeelDefaults().put("OptionPane.messageFont", new Font("Courier New", Font.PLAIN, 12));
				UIManager.getLookAndFeelDefaults().put("OptionPane.buttonFont", new Font("Courier New", Font.PLAIN, 12));
				UIManager.getLookAndFeelDefaults().put("OptionPane.okButtonText", new Font("Courier New", Font.PLAIN, 12));

				Object message =  ois.readObject();
				
				//Login and signup case
				if(message instanceof AuthenticationMessage){ 
					if(((AuthenticationMessage) message).getID() == 0){//login case
						if(((AuthenticationMessage) message).getPassword().equals("success")){
							setUsername(((AuthenticationMessage) message).getUsername());
							mfGUI.displayLobby();
							mfGUI.setTitle();
						}else{
							if(((AuthenticationMessage) message).getErrorType() == Constants.IN_PLAY)
								JOptionPane.showMessageDialog(null, "Player already in play", "Error", JOptionPane.ERROR_MESSAGE);
							else if(((AuthenticationMessage) message).getErrorType() == Constants.ATTEMPT_FAIL)
								JOptionPane.showMessageDialog(null, "Incorrect username or password", "Error", JOptionPane.ERROR_MESSAGE);
							mfGUI.clearLogin();
						}
					} 
					//Signup case
					else {
						if(((AuthenticationMessage) message).getPassword().equals("success")){
							JOptionPane.showMessageDialog(null, "Account Created", "Success", JOptionPane.INFORMATION_MESSAGE);
							mfGUI.clearCreateAccount();
						}else{

							JOptionPane.showMessageDialog(null, "Username already exists", "Error", JOptionPane.ERROR_MESSAGE);
							mfGUI.clearCreateAccount();
						}
					}	
				} else if(message instanceof Player){ 
					if(!exists(((Player)message).getUsername())) {
						playerList.add(((Player)message).getUsername());
						mfGUI.addPlayerToLobby(((Player)message).getUsername());
					}
				} else if(message instanceof GameState) {
					if(firstGameState){
						mfGUI.playSound("countdown");
						try {
							Thread.sleep(3000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						mfGUI.getSoundManager().startBGM("GameBGM");
						mfGUI.displayGameGUI((GameState)message);
						firstGameState = false;
					}
					else if(((GameState) message).isGameOver()) {
						System.out.println("winners are..."+((GameState) message).getWinners());
						System.out.println("42 IS THE BEST NUMBER");
						mfGUI.sendMessage("game is over");
						mfGUI.getRGUI().setGameState((GameState) message);
						mfGUI.getRGUI().populateResultGUI(); 
						mfGUI.displayResult();
					}
					else
						mfGUI.displayGameGUI((GameState)message);
				} else if(message instanceof InvalidMoveMessage) {
					JOptionPane.showMessageDialog(null, ((InvalidMoveMessage) message).getCardTitle() +  " cannot be played on " +
							((InvalidMoveMessage) message).getTarget(), "Invalid move", JOptionPane.ERROR_MESSAGE);
				} else if(message instanceof GetHackedMessage) {
					if(username.equals(((GetHackedMessage) message).getTarget()))
						mfGUI.messUpScreen();
				} else if(message instanceof RickRollMessage) {
					if(username.equals(((RickRollMessage)message).getTarget())) {
						//Pause background music
						mfGUI.getSoundManager().pauseBGM();
						mfGUI.rickRoll();
					}
				} else if((Map<String,ArrayList<Tuple>>) message instanceof Map<?,?>) {
					mfGUI.getRGUI().setAllStats((Map<String, ArrayList<Tuple>>) message);
				} 
			}
		} catch(ClassNotFoundException cnfe){
			System.out.println("Client cnfe: " + cnfe.getMessage());
		} catch (IOException e) {
			System.out.println("Client ioe: " + e.getMessage());
		}
	}
	
	//Sends message encoded with username and password, as well as type (login/signup)
	public void sendAuthenticationMessage(String user, String pass, int ID, int errorType){
		try { 
			AuthenticationMessage as = new AuthenticationMessage(user, pass, ID, errorType);
			oos.reset();
			oos.writeObject(as);
			oos.flush();
		} catch(IOException ioe) {
			System.out.println("Client ioe: " + ioe.getMessage());
		}
	}
	
	//Send when user presses start
	public void sendMessage(String message){
		System.out.println("inside client's sendmessage function, about to write to oos");
		try {
			oos.reset();
			oos.writeObject(message);
			oos.flush();
		} catch(IOException ioe) {
			System.out.println("Client ioe: " + ioe.getMessage());
		}
	}
	
	//Send when user presses ready
	public void sendReadyMessage() {
		try {
			ReadyMessage rm = new ReadyMessage(username);
			oos.reset();
			oos.writeObject(rm);;
			oos.flush();
		} catch(IOException ioe) {
			System.out.println("Client ioe: " + ioe.getMessage());
		}
	}

	//Get username
	public String getUsername() {
		return username;
	}

	//Set username
	public void setUsername(String username) {
		this.username = username;
	}
	
	//Play sound
	public void playSound(String sound) {
		sm.playSound(sound);
	}
	
	//Get player list size
	public int getPlayerListSize(){
		return playerList.size();	
	}
	
	//Send a card message to server
	public void sendCardMessage(Card card, String caster, String target) {
		try {
			CardMessage cm = new CardMessage(card,caster, target);
			oos.reset();
			oos.writeObject(cm);
			oos.flush();
		} catch(IOException ioe) {
			System.out.println("Client sendcardmessage ioe: " + ioe.getMessage());
		}
	}
	
	//Send end turn message to server
	public void sendEndTurnMessage(Player player) {
		try {
			EndTurnMessage etm = new EndTurnMessage(player);
			oos.reset();
			oos.writeObject(etm);
			oos.flush();
		} catch(IOException ioe) {
			System.out.println("Client endturnmessage ioe: " + ioe.getMessage());
		}
	}
	
	//Check if username exists
	public boolean exists(String username) {
		for(String s : playerList) {
			if(username.equals(s))
				return true;
		}
		return false;
	}
	
	//Main
	public static void main(String[] args) {
		IPPopup ipp = new IPPopup(null);
		if(ipp.getIP()!= null){
			new Client(ipp.getIP(), 6789);
		} else{
			System.out.println("  ___ ___                 __     _________ .__  __         ");
			System.out.println(" /"+ "   |   "+" \\"+"_"+"____    ____ |  | __ "+"\\"+"_   ___ "+"\\"+"|__|"+"/  |_ ___.__.");
			System.out.println("/    ~     "+"\\"+"__  "+"\\ "+"_"+"/ ___"+"\\"+"|  |/ / /    "+"\\"+"  \\"+"/|  "+"\\"+"   __<   |  |");
			System.out.println("\\"+"    Y     // __ "+"\\\\  \\___|    <  "+"\\     \\___|  ||  |  \\___  |");
			System.out.println(" \\___|_   /(____  /\\___  >__|_ \\  \\______  /__||__|  / ____|");
			System.out.println("        \\"+"/      \\"+"/     \\"+"/     \\"+"/         \\"+"/          \\"+"/     ");
		}
	}
	
}