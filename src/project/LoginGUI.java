package project;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;

import util.Encryption;

//Login GUI
public class LoginGUI{	
	private JLabel Title,usernameLabel,passwordLabel;
	private JTextField usernameField, passwordField;
	private JButton signIn, guest, createAccount;
	private JPanel basePanel, titlePanel, southPanel;
	private MainFrameGUI main;
	private SoundButton soundButton;
	
	//Constructor
	public LoginGUI(MainFrameGUI main) {
		this.main = main;
		//Set the UI
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			UIManager.getLookAndFeelDefaults().put("Label.font", new Font("Courier New", Font.PLAIN, 16));
			UIManager.getLookAndFeelDefaults().put("Label.foreground", new Color(0,255,0));
			UIManager.getLookAndFeelDefaults().put("Button.font", new Font("Courier New", Font.PLAIN, 12));
			UIManager.getLookAndFeelDefaults().put("Button.foreground", new Color(0,255,0));
			UIManager.getLookAndFeelDefaults().put("Button.background", new Color(50,50,50));
			UIManager.getLookAndFeelDefaults().put("Button.border", new Color(50,50,50));
			UIManager.getLookAndFeelDefaults().put("TextField.background", new Color(50,50,50));
			UIManager.getLookAndFeelDefaults().put("PasswordField.background", new Color(50,50,50));
			UIManager.getLookAndFeelDefaults().put("TextField.foreground", new Color(0,255,0));
			UIManager.getLookAndFeelDefaults().put("PasswordField.foreground", new Color(0,255,0));
			UIManager.getLookAndFeelDefaults().put("TextField.font", new Font("Courier New", Font.PLAIN, 14));
			UIManager.getLookAndFeelDefaults().put("PasswordField.font", new Font("Courier New", Font.PLAIN, 14));
			UIManager.put("TextField.border", new Color(0,0,0));
			UIManager.put("PasswordField.border", new Color(0,0,0));
			UIManager.put("TextField.caretForeground", new Color(0,255,0));
			UIManager.put("PasswordField.caretForeground", new Color(0,255,0));
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		initializeLoginVariables();
		createGUI();
		addActionAdapters();
	}
	
	//Initialize variables
	private void initializeLoginVariables(){
		Title = new JLabel("<LOGIN/>");
		Title.setFont(new Font("Courier New", Font.PLAIN, 69));
		usernameLabel = new JLabel("<Username>: ");
		passwordLabel = new JLabel("<Password>: ");
		usernameField = new JTextField();
		usernameField.setPreferredSize(new Dimension(200,24));
		passwordField = new JPasswordField();
		passwordField.setPreferredSize(new Dimension(200,24));
		signIn = new JButton("<Sign In>");
		guest = new JButton("<Guest Login>");
		createAccount = new JButton("<Create Account>");	
		soundButton = new SoundButton(main.getSoundManager());
		soundButton.setPreferredSize(new Dimension(30,25));
		signIn.setPreferredSize(new Dimension(162,25));
		guest.setPreferredSize(new Dimension(162,25));
		createAccount.setPreferredSize(new Dimension(330,25));
		basePanel = new JPanel();
		southPanel = new JPanel();
		titlePanel = new JPanel();
	};
	
	//Create the GUI
	private void createGUI(){
		basePanel.setLayout(new BoxLayout(basePanel, BoxLayout.Y_AXIS));
		basePanel.setBackground(new Color(0,0,0));
		
		titlePanel.setBackground(new Color(0,0,0));
		titlePanel.add(Title);
		titlePanel.setMaximumSize(titlePanel.getPreferredSize());
		
		JPanel blankPanel = new JPanel();
		blankPanel.setBackground(new Color(0,0,0));
		blankPanel.add(new JLabel(" "));
		blankPanel.setMaximumSize(blankPanel.getPreferredSize());
		basePanel.add(blankPanel);
		
		JPanel userPanel = new JPanel();
		userPanel.setBackground(new Color(0,0,0));
		userPanel.add(usernameLabel);
		userPanel.add(usernameField);
		userPanel.setMaximumSize( userPanel.getPreferredSize());
		basePanel.add(userPanel);
		
		JPanel passwordPanel = new JPanel();
		passwordPanel.setBackground(new Color(0,0,0));
		passwordPanel.setLayout(new FlowLayout());
		passwordPanel.add(passwordLabel);
		passwordPanel.add(passwordField);
		passwordPanel.setMaximumSize(passwordPanel.getPreferredSize());
		basePanel.add(passwordPanel);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setBackground(new Color(0,0,0));
		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.add(signIn);
		buttonPanel.add(guest);
		buttonPanel.setMaximumSize(passwordPanel.getPreferredSize());
		basePanel.add(buttonPanel);
		
		JPanel bottomButtonPanel = new JPanel();
		bottomButtonPanel.setBackground(new Color(0,0,0));
		bottomButtonPanel.add(createAccount);
		bottomButtonPanel.setMaximumSize(passwordPanel.getPreferredSize());
		basePanel.add(bottomButtonPanel);
		
		southPanel.setBackground(new Color(0,0,0));
		southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.Y_AXIS));
		southPanel.add(buttonPanel);
		southPanel.add(bottomButtonPanel);
		JPanel soundPanel = new JPanel();
		soundPanel.add(soundButton);
		soundButton.setFont( new Font("Courier New", Font.PLAIN, 12));
		soundButton.setPreferredSize(new Dimension(70,20));
		soundPanel.setBackground(Color.black);
		southPanel.add(soundPanel);
		
	};
	
	//Add actions
	private void addActionAdapters(){
		//Create account button
		createAccount.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent arg0) {
		    	main.playSound("click");
		    	main.displayCreateAccount();
		    }
		});
		
		//Guest button
		guest.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent arg0) {
		    	main.playSound("click");
		    	main.displayLobby();
		    	Random r = new Random();
		    	String name = "anon"+r.nextInt(10)+r.nextInt(10)+r.nextInt(10)+r.nextInt(10);
		    	main.setUsername(name);
		    	main.sendMessage("PlayerListRequest");
		    	main.sendMessage("Guest Login: " + name);
		    	main.setTitle();
		    }
		});
		
		//Sign in button
		signIn.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent arg0) {
		    	main.playSound("click");
		    	//log in case ID = 0
		    	main.sendAuthenticationMessage(usernameField.getText(), Encryption.encrypt(passwordField.getText()), 0, -1);
		    	main.sendMessage("PlayerListRequest");
		    }
		});
	};
	
	//Clears the text fields
	public void clearTextFields(){
		usernameField.setText("");
		passwordField.setText("");
	}
	
	//Gets south panel 
	public JPanel getSouthPanel(){
		return southPanel;
	}
	
	//Gets center panel
	public JPanel getCenterPanel(){
		return basePanel;
	}
	
	//Gets north panel
	public JPanel getNorthPanel(){
		return titlePanel;
	}
}
