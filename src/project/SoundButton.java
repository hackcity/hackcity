package project;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

//Sound Button class for all GUIs
public class SoundButton extends JButton{
	private static final long serialVersionUID = 1L;
	private static boolean isMuted = false;
	private SoundManager soundManager;

	//Sound Button constructor takes in a sound manager
	public SoundButton(SoundManager soundManager) {
		super();
		this.soundManager = soundManager;
		//Set the text based on current status of sound
		if(soundManager.isPlaying())
			setText(util.Constants.MUTE);
		else
			setText(util.Constants.UNMUTE);
		addActions();
	}
	
	//Add action listener when button is clicked
	public void addActions() {
		addActionListener(new SoundListener(this));
	}
	
	//Mute sounds
	public void setMuted(boolean muted) {
		isMuted = muted;
	}
	
	//Get status of volume
	public boolean getMuted() {
		return isMuted;
	}
	
	//Actions for sound button
	class SoundListener implements ActionListener{
		private SoundButton sb;
		public SoundListener(SoundButton sb) {
			this.sb = sb;
		}
		@Override
		//Set the text of the button and mutes/unmutes when pressed
		public void actionPerformed(ActionEvent e) {
			if(soundManager.isPlaying()) {
				soundManager.pauseBGM();
				sb.setText("<unmute>");
			}
			else {
				soundManager.playBGM();
				sb.setText("<mute>");
			}
		}
	}
}
