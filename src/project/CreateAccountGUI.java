package project;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;

import util.Encryption;

//Create account GUI
public class CreateAccountGUI {	
	private JLabel Title,usernameLabel,passwordLabel, repeatLabel;
	private JTextField usernameField, passwordField, repeatField;
	private JButton back, createAccount;
	private SoundButton soundButton;
	private JPanel basePanel, titlePanel, southPanel;
	private MainFrameGUI main;
	
	//Constructor
	public CreateAccountGUI(MainFrameGUI main) {
		this.main = main;
		//Set UI
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			UIManager.getLookAndFeelDefaults().put("Label.font", new Font("Courier New", Font.PLAIN, 18));
			UIManager.getLookAndFeelDefaults().put("Label.foreground", new Color(0,255,0));
			UIManager.getLookAndFeelDefaults().put("Button.font", new Font("Courier New", Font.PLAIN, 14));
			UIManager.getLookAndFeelDefaults().put("Button.foreground", new Color(0,255,0));
			UIManager.getLookAndFeelDefaults().put("Button.background", new Color(50,50,50));
			UIManager.getLookAndFeelDefaults().put("Button.border", new Color(50,50,50));
			UIManager.getLookAndFeelDefaults().put("TextField.background", new Color(50,50,50));
			UIManager.getLookAndFeelDefaults().put("PasswordField.background", new Color(50,50,50));
			UIManager.getLookAndFeelDefaults().put("TextField.foreground", new Color(0,255,0));
			UIManager.getLookAndFeelDefaults().put("PasswordField.foreground", new Color(0,255,0));
			UIManager.getLookAndFeelDefaults().put("TextField.font", new Font("Courier New", Font.PLAIN, 14));
			UIManager.getLookAndFeelDefaults().put("PasswordField.font", new Font("Courier New", Font.PLAIN, 14));
			UIManager.put("TextField.border", new Color(0,0,0));
			UIManager.put("PasswordField.border", new Color(0,0,0));
			UIManager.put("TextField.caretForeground", new Color(0,255,0));
			UIManager.put("PasswordField.caretForeground", new Color(0,255,0));
		} catch(Exception e) {
			e.printStackTrace();
		}
		initializeVariables();
		createGUI();
		addActionAdapters();
	}

	//Initialize variables
	private void initializeVariables(){
		Title = new JLabel("account.create()");
		Title.setFont(new Font("Courier New", Font.PLAIN, 35));
		usernameLabel = new JLabel("<Username>: ");
		passwordLabel = new JLabel("<Password>: ");
		repeatLabel = new JLabel("  <Repeat>: ");
		usernameField = new JTextField();
		usernameField.setPreferredSize(new Dimension(200,24));
		passwordField = new JPasswordField();
		passwordField.setPreferredSize(new Dimension(200,24));
		repeatField = new JPasswordField();
		repeatField.setPreferredSize(new Dimension(200,24));
		back = new JButton("<Back>");
		back.setPreferredSize(new Dimension(330,25));
		createAccount = new JButton("<Create Account>");		
		createAccount.setPreferredSize(new Dimension(330,25));
		soundButton = new SoundButton(main.getSoundManager());
		basePanel = new JPanel();
		titlePanel = new JPanel();
		southPanel = new JPanel();
	};
	
	//Create GUI
	private void createGUI(){
		basePanel.setLayout(new BoxLayout(basePanel, BoxLayout.Y_AXIS));
		basePanel.setBackground(new Color(0,0,0));
			
		titlePanel.setBackground(new Color(0,0,0));
		titlePanel.add(Title);
		titlePanel.setMaximumSize(titlePanel.getPreferredSize());
		
		JPanel blankPanel = new JPanel();
		blankPanel.setBackground(new Color(0,0,0));
		blankPanel.add(new JLabel(" "));
		blankPanel.setMaximumSize(blankPanel.getPreferredSize());
		basePanel.add(blankPanel);
		
		JPanel blankPanel2 = new JPanel();
		blankPanel2.setBackground(new Color(0,0,0));
		blankPanel2.add(new JLabel(" "));
		blankPanel2.setMaximumSize(blankPanel2.getPreferredSize());
		basePanel.add(blankPanel2);
		
		JPanel userPanel = new JPanel();
		userPanel.setBackground(new Color(0,0,0));
		userPanel.add(usernameLabel);
		userPanel.add(usernameField);
		userPanel.setMaximumSize(userPanel.getPreferredSize());
		basePanel.add(userPanel);
		
		JPanel passwordPanel = new JPanel();
		passwordPanel.setBackground(new Color(0,0,0));
		passwordPanel.setLayout(new FlowLayout());
		passwordPanel.add(passwordLabel);
		passwordPanel.add(passwordField);
		passwordPanel.setMaximumSize(passwordPanel.getPreferredSize());
		basePanel.add(passwordPanel);

		JPanel repeatPanel = new JPanel();
		repeatPanel.setBackground(new Color(0,0,0));
		repeatPanel.setLayout(new FlowLayout());
		repeatPanel.add(repeatLabel);
		repeatPanel.add(repeatField);
		repeatPanel.setMaximumSize(repeatPanel.getPreferredSize());
		basePanel.add(repeatPanel);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setBackground(new Color(0,0,0));
		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.add(createAccount);
		buttonPanel.setMaximumSize(passwordPanel.getPreferredSize());
		
		JPanel bottomButtonPanel = new JPanel();
		bottomButtonPanel.setBackground(new Color(0,0,0));
		bottomButtonPanel.add(back);
		bottomButtonPanel.setMaximumSize(passwordPanel.getPreferredSize());
		
		southPanel.setBackground(new Color(0,0,0));
		southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.Y_AXIS));
		southPanel.add(buttonPanel);
		southPanel.add(bottomButtonPanel);
		JPanel soundPanel = new JPanel();
		soundButton.setFont( new Font("Courier New", Font.PLAIN, 12));
		soundButton.setPreferredSize(new Dimension(70,20));
		soundPanel.add(soundButton);
		soundPanel.setBackground(Color.black);
		southPanel.add(soundPanel);
		
	};
	
	//Add actions
	private void addActionAdapters(){
		//Goes back to login page
		back.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent arg0) {
		    	main.playSound("click");
		    	main.displayLogin();
		    }
		});
		
		//Creates an account
		createAccount.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent arg0) {
		    	//Play sound
		    	main.playSound("click");
		    	//If empty username field, send error
		    	if(usernameField.getText().length() == 0){
		    		JOptionPane.showMessageDialog(null, "Invalid Username", "Failed", JOptionPane.ERROR_MESSAGE);
		    		return;
		    	}
		    	//If empty password field, send error
		    	if(passwordField.getText().length() == 0){
		    		JOptionPane.showMessageDialog(null, "Invalid Password", "Failed", JOptionPane.ERROR_MESSAGE);
		    		return;
		    	}
		    	//Check if password match
		    	if(passwordField.getText().equals(repeatField.getText())){
		    		main.sendAuthenticationMessage(usernameField.getText(), Encryption.encrypt(repeatField.getText()), 1, -1);//encrypt password here
		    	} else{
		    		JOptionPane.showMessageDialog(null, "Password and Repeat do not match", "FAILED", JOptionPane.ERROR_MESSAGE);
		    		return;
		    	}
		    }
		});
	};
	
	//Clear text fields
	public void clearTextfields(){
		usernameField.setText("");
		passwordField.setText("");
		repeatField.setText("");
	}
	
	//Gets south panel
	public JPanel getSouthPanel(){
		return southPanel;
	}
	
	//Gets center panel
	public JPanel getCenterPanel(){
		return basePanel;
	}
	
	//Get north panel
	public JPanel getNorthPanel(){
		return titlePanel;
	}
	
	//Get sound button
	public SoundButton getSoundButton() {
		return soundButton;
	}
}
