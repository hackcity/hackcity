package project;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import dataTypes.Card;
import dataTypes.GameState;
import dataTypes.Player;
import util.Constants;
import util.ImageLibrary;

//GameGUI
public class GameGUI {
	
	private JPanel basePanel;
	private JPanel titlePanel; 
	private JPanel southPanel;
	private JButton endTurn; 
	private SoundButton soundButton;
	private MainFrameGUI main;
	private GameState currentGameState;
	
	//Constructor
	public GameGUI(MainFrameGUI main) {
		this.main = main;		
		UIManager.getLookAndFeelDefaults().put("Label.font", new Font("Courier New", Font.PLAIN, 16));
		UIManager.getLookAndFeelDefaults().put("TextArea.font", new Font("Courier New", Font.PLAIN, 11));
		UIManager.getLookAndFeelDefaults().put("TextArea.foreground", new Color(0,255,0));
		UIManager.getLookAndFeelDefaults().put("Label.foreground", new Color(0,255,0));
		UIManager.getLookAndFeelDefaults().put("Button.font", new Font("Courier New", Font.PLAIN, 16));
		UIManager.getLookAndFeelDefaults().put("Button.foreground", new Color(0,255,0));
		UIManager.getLookAndFeelDefaults().put("Button.background", new Color(50,50,50));
		UIManager.getLookAndFeelDefaults().put("Button.border", new Color(50,50,50));
		UIManager.getLookAndFeelDefaults().put("Panel.background", new Color(0,0,0));
	}
	
	
	//Function to create hand as a jLabel
	public JPanel createHandGUI(List<Card> hand) {
		JPanel panel = new JPanel();
		for(int i=0;i<hand.size();i++) {
			JPanel inner = createCardGUI(hand.get(i),true);
			panel.add(inner);
		}
		return panel;
	}
	
	//Function to create equip as a jLabel (have some room between the cards
	public JPanel createEquipGUI(List<Card> equipped) {
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 35, 10));
		for(int i=0;i<equipped.size();i++) {
			JPanel inner = createCardGUI(equipped.get(i),false);
			panel.add(inner);
		}
		return panel;
	}
	
	//Function to create a jlabel with players (including their health)
	//Takes in a list of players (not including the current player)
	//Note: make sure to call this AFTER REMOVING CURRENT PLAYER FROM THE LIST
	public JPanel createPlayersGUI(List<Player> players) {
		Image img = ImageLibrary.getImage("images/gfx_green.png");
		Image img2 = ImageLibrary.getImage("images/gfx_red.png");
		Image newImage = img.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
		Image newImage2 = img2.getScaledInstance(20, 20, java.awt.Image.SCALE_SMOOTH);
		ImageIcon pic = new ImageIcon(newImage);
		ImageIcon pic2 = new ImageIcon(newImage2);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));
		
		int i = 0;
		for(/*depends on i*/;i < players.size();i++) {
			JPanel innerPanel = new JPanel();
			innerPanel.setLayout(new BoxLayout(innerPanel,BoxLayout.X_AXIS));
			innerPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
			
			JPanel leftPanel = new JPanel();
			
			JLabel label = new JLabel(players.get(i).getUsername());
			if(currentGameState.getCurrentPlayer().getUsername().equals(players.get(i).getUsername()))
			{
				label.setText(">"+players.get(i).getUsername());
			}
			
			
			//Player is dead
			if(players.get(i).isAlive() == false)
				label.setForeground(Color.red);
			
			leftPanel.add(label);
			leftPanel.setPreferredSize(new Dimension(10,25));
			
			innerPanel.add(leftPanel);
					
			JPanel rightPanel = new JPanel();
			rightPanel.setLayout(new BoxLayout(rightPanel,BoxLayout.X_AXIS));
			rightPanel.setPreferredSize(new Dimension(100,25));
			
			rightPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
			for(int j=0;j<players.get(i).getHealth();j++) {
				rightPanel.add(new JLabel(pic));
			}
			for(int j = players.get(i).getHealth(); j < players.get(i).getMaxHealth(); j++){
				rightPanel.add(new JLabel(pic2));
			}
			innerPanel.add(rightPanel);
			
			panel.add(innerPanel);
		}
		for(/*depends on i*/; i < Constants.MAX_PLAYERS; i++){ // fill up empty space
			JPanel innerPanel = new JPanel();
			innerPanel.setLayout(new BoxLayout(innerPanel,BoxLayout.X_AXIS));
			innerPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

			JPanel leftPanel = new JPanel();
			JLabel label = new JLabel("     ");
			leftPanel.add(label);
			leftPanel.setPreferredSize(leftPanel.getMinimumSize());
			
			JPanel rightPanel = new JPanel();
			rightPanel.setLayout(new BoxLayout(rightPanel,BoxLayout.X_AXIS));
			rightPanel.setPreferredSize(new Dimension(100,25));
			
			innerPanel.add(rightPanel);
			
			panel.add(innerPanel);
		}
		
		panel.setPreferredSize(new Dimension(200,250));
		return panel;
	}
	
	
	//Takes in a gamestate, and calls the other functons above and then puts those labels in the right place
	public void createGameGUI(GameState gameState) {

		
		endTurn = new JButton("  turn.end()  ");
		currentGameState = gameState;
		titlePanel = new JPanel();
		titlePanel.setLayout(new BoxLayout(titlePanel, BoxLayout.X_AXIS));
		JPanel players = createPlayersGUI(gameState.getPlayers());
		players.setMaximumSize(new Dimension(200, 300));
		titlePanel.add(players);
		
		JPanel card = new JPanel();
		JPanel label = new JPanel();
		label.add(new JLabel("Last Played: "));
		label.setPreferredSize(label.getMinimumSize());
		card.setLayout(new BoxLayout(card, BoxLayout.Y_AXIS));
		card.add(label);
		card.add(Box.createRigidArea(new Dimension(0, 10)));
		
		JPanel turnPanel = new JPanel();
		turnPanel.setLayout(new BoxLayout(turnPanel,BoxLayout.Y_AXIS));
		JLabel turnLabel = new JLabel();
		JLabel nameLabel = new JLabel();
		turnLabel.setPreferredSize(new Dimension(200,25));
		turnLabel.setText("Current Turn: ");
		nameLabel.setText(gameState.getCurrentPlayer().getUsername());
		turnPanel.add(turnLabel);
		turnPanel.add(nameLabel);
		
		card.add(createCardGUI(gameState.getLastPlayedCard(),false));
		
		card.setMaximumSize(new Dimension(102, 300));
		titlePanel.add(Box.createHorizontalGlue());
		titlePanel.add(card);
		titlePanel.add(Box.createHorizontalGlue());
		titlePanel.add(turnPanel);
		
		southPanel = new JPanel();
		JPanel equipLabel = new JPanel();
		equipLabel.add(new JLabel("Cards Equipped: "));
		equipLabel.setPreferredSize(equipLabel.getMinimumSize());
		JPanel HandLabel = new JPanel();
		HandLabel.add(new JLabel("Your Hand: "));
		HandLabel.setPreferredSize(HandLabel.getMinimumSize());
		
		southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.Y_AXIS));
		southPanel.add(equipLabel);
		southPanel.add(createEquipGUI(gameState.getPlayer(main.getUsername()).getEquippedCards()));
		southPanel.add(HandLabel);
		
		if(gameState.getPlayer(main.getUsername()).isAlive()){
			southPanel.add(createHandGUI(gameState.getPlayer(main.getUsername()).getHand()));
		}else{
			JPanel youDEAD = new JPanel();
			JLabel youDED = new JLabel("YOU DEAD");
			youDED.setFont(new Font("Courier New", Font.BOLD, 50));
			youDED.setForeground(Color.red);
			youDEAD.add(youDED);
			youDEAD.setPreferredSize(youDEAD.getPreferredSize());
			southPanel.add(new JLabel(" "));
			southPanel.add(new JLabel(" "));
			southPanel.add(youDEAD);
			southPanel.add(new JLabel(" "));
			southPanel.add(new JLabel(" "));
		}
		
		soundButton = new SoundButton(main.getSoundManager());
		
		//End turn button
		endTurn.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent arg0) {
		    	//Checks if it's the current window's turn
		    	if(currentGameState.isTurn(main.getUsername()))
		    		main.sendEndTurnMessage(currentGameState.getCurrentPlayer());
		    }
		});
		
		JPanel turnButtonPanel = new JPanel();
		turnButtonPanel.add(endTurn);		
		turnButtonPanel.setFont( new Font("Courier New", Font.PLAIN, 14));
		turnButtonPanel.setPreferredSize(new Dimension(100,25));
		turnButtonPanel.setBackground(Color.black);
		southPanel.add(turnButtonPanel);
		
		JPanel soundPanel = new JPanel();
		soundPanel.add(soundButton);
		soundButton.setFont( new Font("Courier New", Font.PLAIN, 12));
		soundButton.setPreferredSize(new Dimension(70,20));
		soundPanel.setBackground(Color.black);
		southPanel.add(soundPanel);
		
		basePanel = new JPanel();
	}
	
	//Create Card GUI
	public JPanel createCardGUI(Card card, boolean b) {
		JPanel panel = new JPanel(new BorderLayout());
		
		JPanel top = new JPanel();
		JLabel cardName = new JLabel(card.getTitle());
		top.add(cardName);
		
		JPanel bot = new JPanel();
		JTextArea description = new JTextArea(card.getFlavorText());
		description.setEditable(false);  
		description.setCursor(new Cursor(Cursor.HAND_CURSOR));  
		description.setOpaque(false);  
		description.setFocusable(true);
		description.setLineWrap(true);
		description.setWrapStyleWord(true);
		description.setToolTipText(card.getTooltipText());
		if(b){
			description.addMouseListener(new MouseAdapter(){
				@Override
				public void mouseClicked(MouseEvent arg0) {
					if(card.canPlayOnOthers()){
						ArrayList<String> aliveList = new ArrayList<String>();
						for(Player p: currentGameState.getAlivePlayers()){
							if(!p.getUsername().equals(main.getUsername())){
								aliveList.add(p.getUsername());
							}
						}
						PlayerChooserPopup pcp = new PlayerChooserPopup(main,aliveList);
						if(pcp.getSelected()== null){return;}
						main.sendCardMessage(card, main.getUsername(), pcp.getSelected());
					}
					//Card that impacts only the player
					else {
						main.sendCardMessage(card, main.getUsername(), main.getUsername());
					}
				}
	
				@Override
				public void mouseEntered(MouseEvent e) {
					description.setCursor(new Cursor(Cursor.HAND_CURSOR));
					panel.setBorder(BorderFactory.createLineBorder(Color.red));
					cardName.setForeground(Color.red);
					description.setForeground(Color.red);
					cardName.setForeground(Color.red);
				}
				
				@Override
				public void mouseExited(MouseEvent e) {
					panel.setBorder(BorderFactory.createLineBorder(new Color(0,255,0)));
					cardName.setForeground(new Color(0,255,0));
					description.setForeground(new Color(0,255,0));
					cardName.setForeground(new Color(0,255,0));
				}
			});
		}
		
		bot.add(description);
		
		panel.add(top,BorderLayout.NORTH);
		panel.add(bot,BorderLayout.SOUTH);
		
		panel.setBorder(BorderFactory.createLineBorder(new Color(0,255,0)));
		panel.setPreferredSize(new Dimension(110,150));
		
		
		if(b){
			panel.addMouseListener(new MouseAdapter(){
				@Override
				public void mouseClicked(MouseEvent arg0) {
					if(card.canPlayOnOthers()){
						//Sends card message
						ArrayList<String> aliveList = new ArrayList<String>();
						for(Player p: currentGameState.getAlivePlayers()){
							if(!p.getUsername().equals(main.getUsername())){
								aliveList.add(p.getUsername());
							}
						}
						PlayerChooserPopup pcp = new PlayerChooserPopup(main,aliveList);
						if(pcp.getSelected()== null){return;}
						main.sendCardMessage(card, main.getUsername(), pcp.getSelected());
					}
					//Card that impacts only the player
					else {
						main.sendCardMessage(card, main.getUsername(), main.getUsername());
					}
				}
	
				@Override
				public void mouseEntered(MouseEvent e) {
					panel.setCursor(new Cursor(Cursor.HAND_CURSOR));
					panel.setBorder(BorderFactory.createLineBorder(Color.red));
					cardName.setForeground(Color.red);
					description.setForeground(Color.red);
					cardName.setForeground(Color.red);
				}
	
				@Override
				public void mouseExited(MouseEvent e) {
					panel.setBorder(BorderFactory.createLineBorder(new Color(0,255,0)));
					cardName.setForeground(new Color(0,255,0));
					description.setForeground(new Color(0,255,0));
					cardName.setForeground(new Color(0,255,0));
				}
			});
		}
		
		return panel;
	}
	
	//Get south panel
	public JPanel getSouthPanel(){
		return southPanel;
	}
	
	//Get center panel
	public JPanel getCenterPanel(){
		return basePanel;
	}
	
	//Get north panel
	public JPanel getNorthPanel(){
		return titlePanel;
	}
	
	//Get sound button
	public SoundButton getSoundButton() {
		return soundButton;
	}
}
