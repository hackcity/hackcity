package project;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;

//Player chooser popup screen
public class PlayerChooserPopup extends JDialog{

	private static final long serialVersionUID = 1L;
	private ArrayList<String> fileNames;
	private JLabel selectLabel = new JLabel("  Choose A Player");
	private JButton openButton = new JButton("Choose");
	private JButton cancelButton = new JButton("Cancel");
	private JPanel basePanel = new JPanel();
	@SuppressWarnings("rawtypes")
	private JList list;
	private JScrollPane scroll;
	private Font normal;
	private String selected;

	//Constructor
	public PlayerChooserPopup(JFrame a, ArrayList<String> fileNames){
		//Create popup
		super(a, true);
		
		//Set UI
		UIManager.getLookAndFeelDefaults().put("List.font", new Font("Courier New", Font.PLAIN, 16));
		UIManager.getLookAndFeelDefaults().put("List.foreground", new Color(0,255,0));
		UIManager.getLookAndFeelDefaults().put("List.background", new Color(50,50,50));
		
		UIManager.getLookAndFeelDefaults().put("List.selectionForeground", Color.black);
		UIManager.getLookAndFeelDefaults().put("List.selectionBackground", new Color(0,255,0));
		
		//Initialize variables
		this.setTitle("Player Chooser");
		this.setBounds(200, 300, 270, 300);
		this.fileNames = fileNames;
		createGUI();
		addActions();
		this.setVisible(true);
		this.setResizable(false);
	}
	
	//Create GUI
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void createGUI(){
		basePanel.setLayout(new BoxLayout(basePanel, BoxLayout.Y_AXIS));
	
		JPanel top = new JPanel();
		top.setLayout(new FlowLayout(FlowLayout.LEFT));
		selectLabel.setFont(normal);
		top.add(selectLabel);
		basePanel.add(top);
		
		JPanel middle = new JPanel();
		list = new JList(fileNames.toArray());
		list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		list.setVisibleRowCount(-1);
		scroll= new JScrollPane(list);
		scroll.setPreferredSize(new Dimension(200, 150));
		middle.add(scroll); // jtable here
		middle.setMaximumSize(middle.getPreferredSize());
		basePanel.add(middle);
		
		JPanel bot = new JPanel();
		bot.setLayout(new FlowLayout());
		bot.setMaximumSize(bot.getPreferredSize());
		basePanel.add(bot);
		
		JPanel but = new JPanel();
		but.setLayout(new FlowLayout(FlowLayout.RIGHT));
		cancelButton.setFont(normal);
		openButton.setFont(normal);
		but.add(cancelButton);
		but.add(openButton);
		but.add(new JLabel("  "));
		basePanel.add(but);
		
		add(basePanel);
	}
	
	//Add actions
	private void addActions() {
		//Cancel button
		cancelButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent arg0) {
		    	dispose();
		    }
		});
		
		//Selects the player
		openButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent arg0) {
		    	if(list.getSelectedIndex() != -1){
		    		selected = fileNames.get(list.getSelectedIndex());
		    		dispose();
		    		return;
		    	}	
		    	//Error popup if nobody is chosen
		    	else{
		    		JOptionPane.showMessageDialog(null, "Nothing Selected", "ERROR", JOptionPane.ERROR_MESSAGE);
	    			return;
		    	}
		    }
		});
	}
	
	//Gets the selected player
	public String getSelected() {
		return selected;
	}
}