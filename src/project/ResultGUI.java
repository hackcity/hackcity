package project;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;

import dataTypes.GameState;
import dataTypes.Player;
import util.Tuple;

//Result GUI (displayed at the end of game)
public class ResultGUI {
	
	private MainFrameGUI main;
	private JPanel basePanel, titlePanel, southPanel;
	private JLabel titleLabel;
	private JButton closeButton;

	private JLabel winnersLabel, losersLabel, statsLabel;
	private JList<String> winnersList, losersList, statsList;
	private JPanel rightPanel;
	private JScrollPane statsPane;
	
	private GameState gameState = null;
	private Map<String,ArrayList<Tuple>> allStats = null;

	//Constructor
	public ResultGUI(MainFrameGUI main) {
		this.main = main;
		//Set UI
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
			UIManager.getLookAndFeelDefaults().put("Label.font", new Font("Courier New", Font.PLAIN, 18));
			UIManager.getLookAndFeelDefaults().put("Label.foreground", new Color(0,255,0));
			UIManager.getLookAndFeelDefaults().put("Button.font", new Font("Courier New", Font.PLAIN, 16));
			UIManager.getLookAndFeelDefaults().put("Button.foreground", new Color(0,255,0));
			UIManager.getLookAndFeelDefaults().put("Button.background", new Color(50,50,50));
			UIManager.getLookAndFeelDefaults().put("Button.border", new Color(50,50,50));
			UIManager.getLookAndFeelDefaults().put("Panel.background", new Color(0,0,0));
		} catch (Exception e) {
			e.printStackTrace();
		}
		basePanel = new JPanel();
		southPanel = new JPanel();
		titlePanel = new JPanel();
	}
	
	//Populate the result GUI with the stats
	public void populateResultGUI() {
		main.sendMessage("getStats");

		initializeVariables();
		createGUI();
		addActionAdapters();
		
		main.revalidate();
		main.repaint();
	}

	//Initialize variables
	private void initializeVariables() {
		//System.out.println("gamestate is "+gameState.toString());
		titleLabel = new JLabel("Results:");
		titleLabel.setFont(new Font("Courier New", Font.PLAIN, 60));
		closeButton = new JButton("<Exit Game>");
		closeButton.setPreferredSize(new Dimension(330, 30));

		winnersLabel = new JLabel("Winners:");
		losersLabel = new JLabel("Losers:");
		statsLabel = new JLabel(""); // empty for now
		statsLabel.setVisible(false);
		winnersLabel.setFont(new Font("Courier New", Font.PLAIN, 18));
		losersLabel.setFont(new Font("Courier New", Font.PLAIN, 18));
		statsLabel.setFont(new Font("Courier New", Font.PLAIN, 16));

		//Initialize winners
		List<Player> winners;// = new ArrayList<Player>();
		if(gameState != null) {
			//System.out.println("game state is not null");
			winners = gameState.getWinners();
		}
		else {
			winners = new ArrayList<Player>();
		}
		//System.out.println("winners size = "+winners.size());
		String[] winnerArray = new String[winners.size()];
		for (int i = 0; i < winners.size(); i++) {
			winnerArray[i] = winners.get(i).getUsername();
		}
		winnersList = new JList<String>(winnerArray);
		
		//Initialize losers
		List<Player> losers = new ArrayList<Player>();
		if(gameState != null) losers = gameState.getLosers();
		else losers = new ArrayList<Player>();
		String[] loserArray = new String[losers.size()];
		for (int i = 0; i < losers.size(); i++) {
			loserArray[i] = losers.get(i).getUsername();
		}
		losersList = new JList<String>(loserArray);
		
		//Initialized as null, to be set when a user is selected
		statsList = new JList<String>();
	}

	//Creates GUI
	private void createGUI() {
		JPanel titlePanel1 = new JPanel();
		titlePanel1.add(titleLabel);
		titlePanel.add(titlePanel1);

		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));
		leftPanel.add(winnersLabel);
		JScrollPane winnersPane = new JScrollPane(winnersList);
		leftPanel.add(winnersPane);
		winnersPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 5, 5));
		winnersPane.setOpaque(false);
		winnersPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		winnersPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		winnersList.setBackground(Color.darkGray);
		winnersList.setForeground(Color.green);
		winnersList.setSelectionForeground(Color.black);
		winnersList.setSelectionBackground(Color.green);

		leftPanel.add(losersLabel);
		JScrollPane losersPane = new JScrollPane(losersList);
		leftPanel.add(losersPane);
		losersPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 5, 5));
		losersPane.setOpaque(false);
		losersPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		losersPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		losersList.setBackground(Color.darkGray);
		losersList.setForeground(Color.green);
		losersList.setSelectionForeground(Color.black);
		losersList.setSelectionBackground(Color.green);

		rightPanel = new JPanel();
		rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
		rightPanel.add(statsLabel);
		statsPane = new JScrollPane(statsList);
		rightPanel.add(statsPane);
		statsPane.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 10));
		statsPane.setOpaque(false);
		statsPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		statsList.setBackground(Color.darkGray);
		statsList.setForeground(Color.green);
		statsList.setSelectionForeground(Color.black);
		statsList.setSelectionBackground(Color.green);

		basePanel.setLayout(new BoxLayout(basePanel, BoxLayout.X_AXIS));
		basePanel.add(leftPanel);
		basePanel.add(rightPanel);

		southPanel.add(closeButton);
	}

	//Add actions
	private void addActionAdapters() {
		//Closes the resultGUI
		closeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				main.dispose();
				System.exit(0);
			}
		});
		
		//Clicking this list clears losersList
		winnersList.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
				losersList.clearSelection();
				refreshStats();
			}
		});

		//Clicking this list clears winnersList
		losersList.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent me) {
				winnersList.clearSelection();
				refreshStats();
			}
		});
	}

	//Sets the game state
	public void setGameState(GameState gs) {
		System.out.println("inside set game state in rgui");
		gameState = gs;
	}
	
	//Send the stats
	public void setAllStats(Map<String,ArrayList<Tuple>> allStats) {
		this.allStats = allStats;
	}

	//Refresh the stats
	public void refreshStats() {
		//Set the name on the label
		String username = null;
		
		//Get the username
		if(winnersList.isSelectionEmpty() == false)
			username = winnersList.getSelectedValue();
		else
			username = losersList.getSelectedValue();
		
		//Show the text
		statsLabel.setText(username+"'s stats:");
		statsLabel.setVisible(true);
		
		ArrayList<Tuple> stats;
		if(allStats == null) stats = new ArrayList<Tuple>();
		else stats = allStats.get(username);
		
		//Iterate through stats, and generate a list of strings
		List<String> currStatsList = new ArrayList<String>();
		for(Tuple stat : stats) {
			String s = "";
			s += stat.getKey();
			s += ": ";
			s += stat.getValue();
			currStatsList.add(s);
		}
		
		//Set the contents of statsList and update the GUI
		String[] statArray = new String[currStatsList.size()];
		for(int i = 0; i < currStatsList.size(); i++) {
			statArray[i] = currStatsList.get(i);
		}
		
		//Refreshes the GUI
		rightPanel.remove(statsPane);
		statsList = new JList<String>(statArray);
		statsPane = new JScrollPane(statsList);
		statsPane = new JScrollPane(statsList);
		statsPane.setBorder(BorderFactory.createEmptyBorder(0, 5, 5, 10));
		statsPane.setOpaque(false);
		statsPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		statsList.setBackground(Color.darkGray);
		statsList.setForeground(Color.green);
		statsList.setSelectionForeground(Color.black);
		statsList.setSelectionBackground(Color.green);
		rightPanel.add(statsPane);
		main.revalidate();
		main.repaint();
	}

	//Gets south panel
	public JPanel getSouthPanel() {
		return southPanel;
	}

	//Gets center panel
	public JPanel getCenterPanel() {
		return basePanel;
	}

	//Get north panel
	public JPanel getNorthPanel() {
		return titlePanel;
	}
}
