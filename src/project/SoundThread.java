package project;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

//Sound threads (managed by SoundManager)
public class SoundThread extends Thread {

	private String sound;
	private SoundManager sm;
	private boolean loop;
	private Clip clip;
	private int lastFrame;

	//Constructor
	public SoundThread(String sound, SoundManager sm, boolean loop) {
		this.sound = sound;
		this.sm = sm;
		this.loop = loop;
		clip = null;
	}
	
	//Ends the background music
	public void endNow(){
		while(clip == null){return;}
		clip.stop();
	}
	
	//Pauses sound
    public void pause() {
        if (clip != null && clip.isRunning()) {
            lastFrame = clip.getFramePosition();
            clip.stop();
        }
    }

    //Play sound
    public void play() {
        if (clip != null && !clip.isRunning()) {
            // Make sure we haven't passed the end of the file...
            if (lastFrame < clip.getFrameLength()) {
                clip.setFramePosition(lastFrame);
            } else {
                clip.setFramePosition(0);
            }
            clip.start();
        }
    }
    
    //Function for stopping looping
	public void end() {
		loop = false;
	}

	//Running a sound thread
	public void run() {
		try {
			do{
				//If unmuted
				if(sm.isPlaying()) {
					//Play the sound
					AudioInputStream inputStream = AudioSystem.getAudioInputStream(SoundThread.class.getResource("./sounds/" + sound + ".wav"));
					clip = AudioSystem.getClip();
					clip.open(inputStream);
					clip.start();
					Thread.sleep(clip.getMicrosecondLength() / 1000);
				}
			}while(loop);
			
			sm.removeThread(this);
		} catch (UnsupportedAudioFileException uafe) {
			System.out.println(this.toString() + " - uafe: " + uafe.getMessage());
		} catch (IOException ioe) {
			System.out.println(this.toString() + " - ioe: " + ioe.getMessage());
		} catch (LineUnavailableException lue) {
			System.out.println(this.toString() + " - lue: " + lue.getMessage());
		} catch (InterruptedException ie) {
			System.out.println(this.toString() + " - ie: " + ie.getMessage());
		}
	}
}
