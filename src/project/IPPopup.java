package project;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

//IP Popup when the client is run
public class IPPopup extends JDialog{

	private static final long serialVersionUID = 1L;
	private JLabel Label = new JLabel("IP Address:");
	private JButton connectButton = new JButton("Connect");
	private JButton cancelButton = new JButton("Cancel");
	private JTextField textField = new JTextField();
	private JPanel basePanel = new JPanel();
	private String ip;
	
	//Constructor
	public IPPopup(JFrame a){
		super(a, true);
		
		basePanel.setLayout(new BoxLayout(basePanel, BoxLayout.Y_AXIS));
		JPanel top = new JPanel();
		top.setLayout(new FlowLayout(FlowLayout.LEFT));
		top.add(Label);
		basePanel.add(top);
		
		JPanel bot = new JPanel();
		bot.setLayout(new FlowLayout());
		textField.setPreferredSize(new Dimension(250,24));
		bot.add(textField);
		bot.setMaximumSize(bot.getPreferredSize());
		basePanel.add(bot);
		
		JPanel but = new JPanel();
		but.setLayout(new FlowLayout(FlowLayout.RIGHT));
		but.add(connectButton);
		but.add(cancelButton);
		but.add(new JLabel("  "));
		basePanel.add(but);
		
		add(basePanel);
		
		//Cancel button
		cancelButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent arg0) {
		    	dispose();
		    }
		});
		
		//Connect button
		connectButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent arg0) {
		    	if(textField.getText().equals("")){ // textfield not empty
		    			JOptionPane.showMessageDialog(null, "Please enter an IP Address", "ERROR", JOptionPane.ERROR_MESSAGE);
		    			return;
		    	} else{
		    		ip = textField.getText();
		    		dispose();
		    		return;
		    	}	
		    }
		});
		
		this.setTitle("Enter IP Address");
		this.setBounds(300, 300, 300, 150);
		this.setVisible(true);
	}
	
	//Get IP
	public String getIP(){
		return ip;
	}
}
